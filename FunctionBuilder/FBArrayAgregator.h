// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Process array items and get some agregated values from.
//
//===================================================================
//  JUN 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBArrayAgregator_h
#define FBArrayAgregator_h

#include "FBMathUtils.h"

template <class T>
class FBArrayAgregator
{
public:
  FBArrayAgregator()
  {
    reset();
  }
  
  void ProcessValue(T iValue)
  {
    _sum += iValue;
    _min = FBMin(_min, iValue);
    _max = FBMax(_max, iValue);
    _elementCount++;
  }

  void reset()
  {
    _sum = 0;
    _min =  (T)1.00E30;
    _max = -(T)1.00E30;
    _elementCount = 0;
  }

  T GetAverage() const 
  { 
    return _elementCount != 0 ? _sum / _elementCount : 0;
  }

  T GetMax() const { return _max; }
  T GetMin() const { return _min; }
  T GetSum() const { return _sum; }

protected:
  unsigned int _elementCount;

  T _sum;
  T _min;
  T _max;
};


#endif // FBArrayAgregator_h
