// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Functions and macros for safe and powerful checks
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBAssert.h"

FBAssertMode FBAssertSettings::FBCurrentAssertMode = amBreakAndWarning;

