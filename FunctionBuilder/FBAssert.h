// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Functions and macros for safe and powerful checks
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBAssert_h
#define FBAssert_h

#define FB_ASSERTS

#ifdef FB_ASSERTS

#include <sstream>
#include <iostream>

#if defined(_MSC_VER) && _MSC_VER >= 1400
#include <intrin.h>
#endif

enum FBAssertMode
{
  amNone,
  amLogError,
  amLogWarning,
  amBreakAndWarning
};

class FBAssertSettings
{
public:
  static FBAssertMode FBCurrentAssertMode;
};

#define FBAssertError(iExp, ipMessage) \
{                                      \
  if(FBAssertSettings::FBCurrentAssertMode == amBreakAndWarning) { \
    std::cout << "WARNING Assertion failed: " << #iExp << " " << (ipMessage != NULL ? ipMessage : ""); \
    __debugbreak();                    \
  }                                    \
                                       \
  if(FBAssertSettings::FBCurrentAssertMode == amLogError) { \
    std::cout << "ERROR Assertion failed: " << #iExp << " " << (ipMessage != NULL ? ipMessage : ""); \
  }                                    \
                                       \
  if(FBAssertSettings::FBCurrentAssertMode == amLogWarning) { \
    std::cout << "WARNING Assertion failed: " << #iExp << " " << (ipMessage != NULL ? ipMessage : ""); \
  }                                    \
}                                      \

#define FBAssert(exp) \
  if (!(exp)) { \
    FBAssertError(#exp, ""); \
  }

#define FBAssertT(exp, message) \
  if (!(exp)) { \
    std::ostringstream msg; \
    msg << message; \
    FBAssertError(#exp, msg.str().c_str()); \
  }

#define FBAssertR(exp) \
  if (!(exp)) { \
    FBAssertError(#exp, ""); \
    return; \
  }

#define FBAssertV(exp, ret) \
  if (!(exp)) { \
    FBAssertError(#exp, ""); \
    return ret; \
  }

#define FBAssertB(exp) \
  if (!(exp)) { \
    FBAssertError(#exp, ""); \
    break; \
  }

#define FBAssertC(exp) \
  if (!(exp)) { \
    FBAssertError(#exp, ""); \
    continue; \
  }

#define FBFCheckR(exp) \
{\
  FBResult FBFCheckR_res = exp; \
  if (!ok(FBFCheckR_res)) { \
    FBAssertError(#exp, ToString(FBFCheckR_res)); \
    return; \
  } \
}

#define FBFCheckV(exp) \
{\
  FBResult FBFCheckV_res = exp; \
  if (!ok(FBFCheckV_res)) { \
    FBAssertError(#exp, ToString(FBFCheckV_res)); \
    return FBFCheckV_res; \
  }\
}

#define FBFCheckErrorBase(exp, operation) \
{\
  FBResult FBFCheckV_res = exp; \
  if (FBFCheckV_res != rcOk) {   \
    if(FBFCheckV_res != rcFalse) { \
      FBAssertError(#exp, ToString(FBFCheckV_res)); \
    }                     \
    operation;            \
  } \
}

#define FBFCheckError(exp) FBFCheckErrorBase(exp, int i = 0)
#define FBFCheckErrorV(exp) FBFCheckErrorBase(exp, return FBFCheckV_res)
#define FBFCheckErrorR(exp) FBFCheckErrorBase(exp, return)
#define FBFCheckErrorC(exp) FBFCheckErrorBase(exp, continue)


#else

#define FBAssert(exp)

#define FBAssertT(exp, message)

#define FBAssertR(exp) \
  if (!(exp)) { \
    return; \
  }

#define FBAssertV(exp, ret) \
  if (!(exp)) { \
    return ret; \
  }

#define FBAssertB(exp) \
  if (!(exp)) { \
    break; \
  }

#define FBAssertC(exp) \
  if (!(exp)) { \
    continue; \
  }

#define FBFCheckR(exp) \
  if (!ok(exp)) { \
    return; \
  }
}
#define FBFCheckV(exp) \
  FBResult res = exp; \
  if (!ok(res)) { \
    return res; \
  }

#endif // if not defined FB_ASSERTS

#endif // FBAssert_h
