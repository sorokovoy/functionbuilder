// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// define basis of constant function
//
//===================================================================
//  APR 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBConstantBasis_H
#define FBConstantBasis_H

#include "IFBFunctionBasis.h"

class FBConstantBasis : public IFBFunctionBasis
{
public:
  virtual unsigned int GetVarCountPerElement() const { return 0; }
  virtual double CalculateValueByIdx(double iValue, int idx) const { return 0; }
  virtual IFBFunctionBasis* CreateDerivaty() const { return new FBConstantBasis(); }
  virtual IFBFunctionBasis* CreateCopy() const { return new FBConstantBasis(); }
  virtual double TransformDerivatyCoeficients(double* ioCoefs) const { return 0; }
  virtual std::string ShowDependencyType(int idx, const char* varName) const { return "0*" + std::string(varName); }
};


#endif // FBConstantBasis_H
