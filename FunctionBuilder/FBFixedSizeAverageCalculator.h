// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Fast calculation of average on some array
//
//===================================================================
//  JUN 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBFixedSizeAverageCalculator_h
#define FBFixedSizeAverageCalculator_h

#include "FBFixedSizeBuffer.h"

template <class T>
class FBFixedSizeAverageCalculator : public FBFixedSizeBuffer<T>
{
public:
  FBFixedSizeAverageCalculator(int iMaxSize)
    :FBFixedSizeBuffer<T>(iMaxSize, NULL)
  {
    _sum = 0;
  }

  T GetAverage()
  {
    return GetElementNumber() == 0 ? 0 : _sum / GetElementNumber();
  }

protected:
  T _sum;

  virtual void OnElementAdd(T& Value) 
  {
    _sum += Value;
  }

  virtual void OnElementDelete(T& Value)
  {
    _sum -= Value;
  }
};


#endif // FBArrayAgregator_h
