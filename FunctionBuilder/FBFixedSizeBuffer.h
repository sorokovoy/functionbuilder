// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Buffer for store fixed count of elements. On overflow las element
// will be deleted
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBFixedSizeBuffer_H
#define FBFixedSizeBuffer_H

#include "FBAssert.h"

template <class T>
class IFBFixedSizeBufferElementDestructor
{
public:
  virtual void Destroy(T iValue) = 0;
};

template <class T>
class FBFixedSizeBufferSTDPtrDestructor : public IFBFixedSizeBufferElementDestructor<T>
{
public:
  virtual void Destroy(T iValue) { delete iValue; }
};

template <class T>
class FBFixedSizeBuffer
{
public:
  FBFixedSizeBuffer(unsigned int iMaxElementsCount, IFBFixedSizeBufferElementDestructor<T>* cipDestructor = NULL)
  {
    _maxElements = iMaxElementsCount;
    FBAssert(_maxElements != 0);
    _maxElements = _maxElements != 0 ? _maxElements : 1;
    _pStore = new T[_maxElements];
    FBAssert(_pStore);

    _currentIdx = 0;
    _isBufferFull = false;
    _pDestructor = cipDestructor;
  }

  ~FBFixedSizeBuffer()
  {
    if(_pDestructor != NULL) {
      for(unsigned int i = 0; i < (_isBufferFull ? _maxElements : _currentIdx); i++) {
        _pDestructor->Destroy(_pStore[i]);
      }
    }
    delete _pDestructor;
    delete _pStore;
  }

  void clear()
  {
    _currentIdx = 0;
    _isBufferFull = false;
  }

  void AddElement(T iValue)
  {
    if(_isBufferFull && _pDestructor != NULL) {
      OnElementDelete(_pStore[_currentIdx]);
      _pDestructor->Destroy(_pStore[_currentIdx]);
    }

    _pStore[_currentIdx] = iValue;
    OnElementAdd(iValue);
    _currentIdx++;
    if(_currentIdx == _maxElements) {
      _currentIdx = 0;
      _isBufferFull = true;
    }
  }

  // last added element at first index 
  const T& GetValue(unsigned int iIdx) const
  {
    FBAssert(iIdx < _maxElements);
    // calculate Idx in array
    iIdx = (_currentIdx - 1 - iIdx + _maxElements) % _maxElements;
    // check requested value added
    FBAssert( _isBufferFull || iIdx < _currentIdx);
    return _pStore[iIdx];
  }

  T& operator[] (unsigned int i)
  {
    return GetValue(i);
  }

  // check value with specified index exist in collection
  bool IsContains(unsigned int iIdx)
  {
    if(iIdx >= _maxElements)
      return false;
    if(IsFull())
      return true;
    return iIdx < _currentIdx;
  }

  unsigned int GetStorageCapasity() const { return _maxElements; }
  unsigned int GetElementNumber() const 
  {
    if(IsFull())
      return _maxElements;
    return _currentIdx; 
  }

  bool IsFull() const { return _isBufferFull; }

private:
  T* _pStore;
  unsigned int _maxElements;

  unsigned int _currentIdx; 
  bool _isBufferFull;

protected:
  virtual void OnElementAdd(T& Value) {};
  virtual void OnElementDelete(T& Value) {};

  IFBFixedSizeBufferElementDestructor<T>* _pDestructor;
};


#endif // FBFixedSizeBuffer_H
