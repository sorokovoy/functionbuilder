// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Implelmentation of foruer basis.
// sin(x) + cos(x) + sin(2*x) + cos(2*x) + ... + sin((iBasisLenght-1)*x) + cos((iBasisLenght-1)*x)
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBFourierBasis.h"
#include "FBMathUtils.h"

// ========================================= FBFourierBasis =========================================

FBFourierBasis::FBFourierBasis(std::vector<double> iArgMultiplyers, FBFourierBasisType iType)
:_argMultiplyers(iArgMultiplyers),
 _type(iType)
{}

unsigned int FBFourierBasis::GetVarCountPerElement() const
{
  return _argMultiplyers.size() * (_type == eFBFourierBasisTypeSinAndCos ? 2 : 1);
}

double FBFourierBasis::CalculateValueByIdx(double iValue, int idx) const
{
  idx = idx % GetVarCountPerElement();
  switch(_type)
  {
    case eFBFourierBasisTypeOnlySin: return sin(_argMultiplyers[idx] * iValue);
    case eFBFourierBasisTypeOnlyCos: return cos(_argMultiplyers[idx] * iValue);
    case eFBFourierBasisTypeSinAndCos: return (idx % 2 == 0 ? sin(_argMultiplyers[(idx / 2)] * iValue) : cos(_argMultiplyers[(idx / 2)] * iValue));
  }
  return 0;
}

IFBFunctionBasis* FBFourierBasis::CreateDerivaty() const
{
  FBFourierBasisType newBasisType;
  switch(_type) {
    case eFBFourierBasisTypeOnlySin: { newBasisType = eFBFourierBasisTypeOnlyCos; break; }
    case eFBFourierBasisTypeOnlyCos: { newBasisType = eFBFourierBasisTypeOnlySin; break; }
    case eFBFourierBasisTypeSinAndCos: { newBasisType = eFBFourierBasisTypeSinAndCos; break; }
  }
  return new FBFourierBasis(_argMultiplyers, newBasisType);
}

IFBFunctionBasis* FBFourierBasis::CreateCopy() const
{
  return new FBFourierBasis(_argMultiplyers, _type);
}

double FBFourierBasis::TransformDerivatyCoeficients(double* ioCoefs) const
{
  switch(_type)
  {
    case eFBFourierBasisTypeOnlySin: 
      {
        for(unsigned int i = 0; i < _argMultiplyers.size(); i++)
          ioCoefs[i] = ioCoefs[i] * _argMultiplyers[i];
        break;
      }
    case eFBFourierBasisTypeOnlyCos: 
      {
        for(unsigned int i = 0; i < _argMultiplyers.size(); i++)
          ioCoefs[i] =  - ioCoefs[i] * _argMultiplyers[i];
        break;
      }
    case eFBFourierBasisTypeSinAndCos: 
      {
        for(unsigned int i = 0; i < _argMultiplyers.size(); i++)
        {
          ioCoefs[i * 2] = _argMultiplyers[i] * ioCoefs[i * 2];
          ioCoefs[i * 2 + 1] = - _argMultiplyers[i] * ioCoefs[i * 2 + 1];
          FBSwap(ioCoefs[i * 2], ioCoefs[i * 2 + 1]);
        }
        break;
      }
  }
  return 0;
}

std::string FBFourierBasis::ShowDependencyType(int idx, const char* varName) const
{
  char buf[255];
  switch(_type)
  {
    case eFBFourierBasisTypeOnlySin:
      {
        sprintf_s(buf, 255, " Sin(%.3f*%s) ", _argMultiplyers[idx], varName);
        break;
      }
    case eFBFourierBasisTypeOnlyCos:
      {
        sprintf_s(buf, 255, " Cos(%.3f*%s) ", _argMultiplyers[idx], varName);
        break;
      }
    case eFBFourierBasisTypeSinAndCos:
      {
        sprintf_s(buf, 255, " %s(%.3f*%s) ", (idx % 2 == 0 ? "Sin" : "Cos"), _argMultiplyers[idx / 2], varName);
        break;
      }
  }
  return buf;
}
