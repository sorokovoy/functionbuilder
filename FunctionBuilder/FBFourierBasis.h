// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Implelmentation of foruer basis.
// sin(x) + cos(x) + sin(2*x) + cos(2*x) + ... + sin((iBasisLenght-1)*x) + cos((iBasisLenght-1)*x)
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBFourierBasis_h
#define FBFourierBasis_h

#include "IFBFunctionBasis.h"

enum FBFourierBasisType
{
  eFBFourierBasisTypeOnlySin,
  eFBFourierBasisTypeOnlyCos,
  eFBFourierBasisTypeSinAndCos
};

class FBFourierBasis : public IFBFunctionBasis
{
public:
  FBFourierBasis(std::vector<double> iArgMultiplyers, FBFourierBasisType iType);
  const std::vector<double>& GetMutiplyers() const { return _argMultiplyers; }

  virtual unsigned int GetVarCountPerElement() const;
  virtual double CalculateValueByIdx(double iValue, int idx) const;
  virtual IFBFunctionBasis* CreateDerivaty() const;
  virtual IFBFunctionBasis* CreateCopy() const;
  virtual double TransformDerivatyCoeficients(double* ioCoefs) const;
  virtual std::string ShowDependencyType(int idx, const char* varName) const;
private:
  std::vector<double> _argMultiplyers;
  const FBFourierBasisType _type;
};

#endif // FBFourierBasis_h
