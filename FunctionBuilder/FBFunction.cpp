// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Implements storage shell fo function
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "IFBFunctionBasis.h"

#include "FBPolynomialBasis.h"
#include "FBLegendreBasis.h"

#include "FBFunction.h"
#include "FBRangeRect.h"

#include "FBAssert.h"

FBFunctionExtremumInfo::FBFunctionExtremumInfo(FBNDimPoint* cipPoint, FBFunctionExtremumType iIsMax)
:pPoint(cipPoint), type(iIsMax)
{}

FBFunctionExtremumInfo::~FBFunctionExtremumInfo()
{
  delete pPoint;
}

FBFunction::FBFunction(const std::vector<FBAutoPtr<IFBFunctionBasis>>& cipBasis, unsigned int iVarCount, FBAutoPtr<const double> ipCoefs, bool iIsContainsConstant, double iTolerance)
:_usedBasis(cipBasis),
 _variableNumber(iVarCount),
 _isContainsConstant(iIsContainsConstant),
 _tolerance(iTolerance)
{
  _coefs = ipCoefs;

  _elementPerVariable = 0;
  for(unsigned int i = 0; i < _usedBasis.size(); i++)
    _elementPerVariable += _usedBasis[i]->GetVarCountPerElement();

  _derivates = new FBFunction*[_variableNumber];
  for(unsigned int i = 0; i < _variableNumber; i++)
    _derivates[i] = NULL;
}

FBFunction::~FBFunction()
{
  ResetDerivatyFuncValues();
  delete _derivates;
}

bool FBFunction::IsUsedVariable(unsigned int iVarIdx) const
{
  if(iVarIdx >= _variableNumber) return false;
  int idx = iVarIdx * _elementPerVariable;
  for(unsigned int id = 0; id < _elementPerVariable; id++) {
    if(FBAbs(_coefs[idx + id]) > _tolerance)
      return true;
  }
  return false;
}

const FBPolynomialBasis* FBFunction::GetSimplePolinomeBasis() const
{
  if(_usedBasis.size() == 1) {
    const FBPolynomialBasis* pPolinomialBasis = dynamic_cast<const FBPolynomialBasis*>(_usedBasis[0].GetPointer());
    if(pPolinomialBasis != NULL && pPolinomialBasis->IsAllDegsInteger()) {
      return pPolinomialBasis;
    }
  }
  return NULL;
}

void FBFunction::ResetDerivatyFuncValues()
{
  for(unsigned int i = 0; i < _variableNumber; i++) {
    delete _derivates[i];
    _derivates[i] = NULL;
  }

  for(unsigned int i = 0; i < _funcExtremums.size(); i++)
    delete _funcExtremums[i];
  _funcExtremums.clear();
}

bool FBFunction::EvaluateFunctionValue(const double* ipArgument, double& oValue, int iElevateOnlyByVariable) const
{
  oValue = 0;
  unsigned int i = 0;
  for (unsigned int IVar = 0; IVar < _variableNumber; IVar++) {
    if(iElevateOnlyByVariable != -1 && iElevateOnlyByVariable != IVar) {
      i += _elementPerVariable;
      continue;
    }
    for (unsigned int IdestId = 0; IdestId < _usedBasis.size(); IdestId++) {
      const IFBFunctionBasis* IDest = _usedBasis[IdestId];
      for (unsigned int IcurrDestVarId = 0; IcurrDestVarId < IDest->GetVarCountPerElement(); IcurrDestVarId++) {
        oValue += _coefs[i] * IDest->CalculateValueByIdx(ipArgument[IVar], IcurrDestVarId);
        i++;
      }
    }
  }

  if(_isContainsConstant)
    oValue += _coefs[i];

  if (FBIsNAN(oValue)) return false;
  return true;
}

double FBFunction::EvaluateFunctionValue(const double* ipArgument) const
{
  double fVal = 0;
  FBAssert(EvaluateFunctionValue(ipArgument, fVal));
  return fVal;
}

bool FBFunction::EvaluateFunctionValue(FBAutoPtr<const double> ipArgument, double& oFuncValue, int iElevateOnlyByVariable) const
{
  return EvaluateFunctionValue(ipArgument.GetPointer(), oFuncValue, iElevateOnlyByVariable);
}

double FBFunction::EvaluateFunctionValue(FBAutoPtr<const double> ipArgument) const
{
  return EvaluateFunctionValue(ipArgument.GetPointer());
}

bool FBFunction::EvaluateFunctionValue(FBAutoPtr<double> ipArgument, double& oFuncValue, int iElevateOnlyByVariable) const
{
  return EvaluateFunctionValue(ipArgument.GetPointer(), oFuncValue, iElevateOnlyByVariable);
}

double FBFunction::EvaluateFunctionValue(FBAutoPtr<double> ipArgument) const
{
  return EvaluateFunctionValue(ipArgument.GetPointer());
}

bool FBFunction::EvaluateFunctionValue(const FBFunctionArgument& ipArgument, double& oValue, int iElevateOnlyByVariable) const
{
  oValue = 0;

  unsigned int i = 0;
  unsigned int blokIdx = 0;
  unsigned int elementIdx = 0;
  for (unsigned int IVar = 0; IVar < _variableNumber; IVar++) {
    double variableValue = ipArgument.arg[blokIdx].GetData()[elementIdx];
    elementIdx++;
    if(elementIdx== ipArgument.arg[blokIdx].GetDim() && ipArgument.arg.size() != 1) {
      elementIdx = 0;
      blokIdx++;
    }
    if(iElevateOnlyByVariable != -1 && iElevateOnlyByVariable != IVar) {
      i += _elementPerVariable;
      continue;
    }
    for (unsigned int IdestId = 0; IdestId < _usedBasis.size(); IdestId++) {
      const IFBFunctionBasis* IDest = _usedBasis[IdestId];
      for (unsigned int IcurrDestVarId = 0; IcurrDestVarId < IDest->GetVarCountPerElement(); IcurrDestVarId++) {
        oValue += _coefs[i] * IDest->CalculateValueByIdx(variableValue, IcurrDestVarId);
        i++;
      }
    }
  }

  FBAssertV(ipArgument.arg.size() == 1 || blokIdx == ipArgument.arg.size(), false);
  FBAssertV(ipArgument.arg.size() == 1 || elementIdx == ipArgument.arg[blokIdx - 1].GetDim(), false);

  if(_isContainsConstant)
    oValue += _coefs[i];

  if (FBIsNAN(oValue)) return false;
  return true;
}

bool FBFunction::EvaluateFunctionValue(FBFunctionPointData& iArgRes, int iElevateOnlyByVariable) const
{
  return EvaluateFunctionValue(iArgRes.arg, iArgRes.fVal, iElevateOnlyByVariable);
}

double FBFunction::EvaluateFunctionValue(const FBFunctionArgument& ipArgument) const
{
  double fVal = 0;
  FBAssert(EvaluateFunctionValue(ipArgument, fVal));
  return fVal;
}

FBFunction* FBFunction::EvaluateDirivaryFunction(unsigned int iVarIdx)
{
  FBAssertV(iVarIdx < _variableNumber, NULL);

  if(_derivates[iVarIdx] != NULL)
    return _derivates[iVarIdx];

  double* currCoefs = new double[_elementPerVariable + 1];
  currCoefs[_elementPerVariable] = 0;
  unsigned int currCoefIdx = 0;
  std::vector<FBAutoPtr<IFBFunctionBasis>> newBasis;

  for (unsigned int IdestId = 0; IdestId < _usedBasis.size(); IdestId++) {
    const IFBFunctionBasis* IDest = _usedBasis[IdestId];
    double* dataBlockBeginPointer = &currCoefs[currCoefIdx];
    for (unsigned int IcurrDestVarId = 0; IcurrDestVarId < IDest->GetVarCountPerElement(); IcurrDestVarId++) {
      currCoefs[currCoefIdx] = _coefs[ iVarIdx * _elementPerVariable + currCoefIdx];
      currCoefIdx++;
    }
    currCoefs[_elementPerVariable] += IDest->TransformDerivatyCoeficients(dataBlockBeginPointer);
    newBasis.push_back(IDest->CreateDerivaty());
  }

  FBFunction* result = new FBFunction(newBasis, 1, currCoefs, true, _tolerance);
  _derivates[iVarIdx] = result;
  return result;
}

std::string FBFunction::ToString() const
{
  std::string oValue;
  unsigned int i = 0;
  for (unsigned int IVar = 0; IVar < _variableNumber; IVar++) {
    for (unsigned int IdestId = 0; IdestId < _usedBasis.size(); IdestId++) {
      const IFBFunctionBasis* IDest = _usedBasis[IdestId];
      for (unsigned int IcurrDestVarId = 0; IcurrDestVarId < IDest->GetVarCountPerElement(); IcurrDestVarId++) {
        char varName[128];
        sprintf_s(varName, 128, "x%d", IVar);
        char itemBuf[256];
        sprintf_s(itemBuf, 256, "%.4f*%s + ", _coefs[i], IDest->ShowDependencyType(IcurrDestVarId, varName).c_str());
        oValue += itemBuf;
        i++;
      }
    }
  }
  
  if(_isContainsConstant) {
    char constantBuffer[128];
    sprintf_s(constantBuffer, 128, " %.4f", _coefs[i]);
    oValue += std::string(constantBuffer);
  }
  return oValue;
}

std::vector<double> FBFunction::EvaluateRoots(double iMin, double iMax) const
{
  std::vector<double> roots;
  FBAssertV(_variableNumber == 1, roots);
  FBAssertV(FBLess(iMin, iMax, _tolerance), roots);

  if(_usedBasis.size() == 1) {
    const FBPolynomialBasis* pPolinomialBasis = dynamic_cast<const FBPolynomialBasis*>(_usedBasis[0].GetPointer());
    if(pPolinomialBasis != NULL) { // solve polynomial 
      double eqRoots[4];
      double eqCoefs[5];
      for(unsigned int s = 0; s < 5; s ++)
        eqCoefs[s] = 0;

      const std::vector<double>& degs = pPolinomialBasis->GetDegs();
      // validate coefficients 
      if(degs.size() <= 5) {
        bool hasNotValidDeg = false;
        for(unsigned int i = 0; i < degs.size(); i++) {
          if(FBAbs(degs[i] - floor(degs[i] + TOLERANCE)) > 3 * TOLERANCE || FBLess(degs[i], 0) || FBLess(4, degs[i])) {
            hasNotValidDeg = true;
            break;
          } else {
            eqCoefs[4 - (int)(degs[i] + TOLERANCE)] = _coefs[i];
          }
        }
        if(!hasNotValidDeg) {
           unsigned int rootCount = FBSolveFourthDegreePolynomial( eqCoefs, eqRoots, _tolerance);
           for(unsigned int k = 0; k < rootCount; k++)
             if(FBIsInRangeEx(eqRoots[k], iMin, iMax, _tolerance))
               roots.push_back(eqRoots[k]);
           return roots;
        }
      }
    }

    /*
    FBFourierBasis* pFourierBasis = dynamic_cast<FBFourierBasis*>(_usedBasis[0]);
    if(pFourierBasis != NULL) { // solve trigonometrical

    }
    */
  }

  // in general case use bisection method 
  return EvaluateRootsByBisectionMethod(iMin, iMax);
}

std::vector<double> FBFunction::EvaluateRootsByBisectionMethod(double iMin, double iMax) const
{
  std::vector<double> roots;
  FBAssertV(_variableNumber == 1, roots);
  FBAssertV(FBLess(iMin, iMax, _tolerance), roots);


  // in any other case use bisection method;
  unsigned int steps = FBMin((unsigned int) ((iMax - iMin) / FBMax(_tolerance, TOLERANCE)), 1000);
  double step = (iMax - iMin) / steps;

  double currX = iMin;

  double minX = iMin;
  double maxX = iMin;
  double fVal;
  EvaluateFunctionValue(&currX, fVal);
  double sign = FBSign(fVal, _tolerance);

  for(unsigned int i = 1; i < steps; i++) {
    currX += step;
    EvaluateFunctionValue(&currX, fVal);
    double currSign = FBSign(fVal, _tolerance);

    if(sign == currSign && currSign != 0)
      minX = currX;

    if(currSign * sign == -1 || (sign == 0 && currSign != 0)) {
      double minFvalue = sign;
      double maxFvalue = currSign;

      sign = currSign;
      maxX = currX;

      bool error = false;

      // evaluate bisection;
      double middleValue = (minX + maxX)/2;

      EvaluateFunctionValue(&middleValue, fVal);
      while(!error && !FBEqual(fVal, 0, _tolerance)) {
        if(minFvalue * fVal < 0) {
          maxX = middleValue;
          maxFvalue = fVal;
        } else {
        //if(maxFvalue * fVal < 0) {
          minX = middleValue;
          minFvalue = fVal;
        }
        if(FBEqual(minX, maxX, _tolerance))
          error = true;
        middleValue = (minX + maxX)/2;
        EvaluateFunctionValue(&middleValue, fVal);
      }
      minX = currX;
      roots.push_back(middleValue);
    }
  }
  return roots;
}

std::vector<FBFunctionExtremumInfo*>& FBFunction::EvaluateExtremums(const FBRangeRect* ipFunctionDOM, FBFunctionExtremumType iType, std::vector<FBNullableType<double>>* ipFixedValues)
{
  if(_funcExtremums.size() != 0)
    return _funcExtremums;

  // 1 get derivaty roots;
  std::vector<std::vector<unsigned int>> maxPoint;
  std::vector<std::vector<unsigned int>> minPoint;
  std::vector<std::vector<double>> allRoots;

  double* testVars = new double[_variableNumber];
  for(unsigned int i = 0; i < _variableNumber; i++)
    testVars[i] = 0;

  for(unsigned int varIdx = 0; varIdx < _variableNumber; varIdx++) {
    std::vector<double> roots;
    if(ipFixedValues != NULL && ipFixedValues->size() != 0 && (*ipFixedValues)[varIdx].HasValue()) {
      roots.push_back((*ipFixedValues)[varIdx].GetValue());
    } else {
      FBFunction* derivaty = this->EvaluateDirivaryFunction(varIdx);
      roots = derivaty->EvaluateRoots(ipFunctionDOM->GetMines()[varIdx], ipFunctionDOM->GetMaxes()[varIdx]);
      roots.push_back(ipFunctionDOM->GetMines()[varIdx]);
      roots.push_back(ipFunctionDOM->GetMaxes()[varIdx]);
    }

    double fVal;
    testVars[varIdx] = roots[0];
    EvaluateFunctionValue(testVars, fVal);
    double maxValue = fVal;
    std::vector<unsigned int> maxIdxs;
    maxIdxs.push_back(0);

    double minValue = fVal;
    std::vector<unsigned int> minIdxs;
    minIdxs.push_back(0);

    for(unsigned int j = 1; j < roots.size(); j++) {
      testVars[varIdx] = roots[j];
      EvaluateFunctionValue(testVars, fVal);

      if(FBLess(fVal, minValue, _tolerance)) {
        minIdxs.clear();
        minIdxs.push_back(j);
        minValue = fVal;
      } else {
        if(FBEqual(fVal, minValue, _tolerance)) {
          minIdxs.push_back(j);
        }
      }

      if(FBLess(maxValue, fVal, _tolerance)) {
        maxIdxs.clear();
        maxIdxs.push_back(j);
        maxValue = fVal;
      } else {
        if(FBEqual(fVal, maxValue, _tolerance)) {
          maxIdxs.push_back(j);
        }
      }
    }
    testVars[varIdx] = 0;
    maxPoint.push_back(maxIdxs);
    minPoint.push_back(minIdxs);
    allRoots.push_back(roots);
  }
  delete testVars;

  switch(iType)
  {
  case eMax:
    {
      GeneratePointEnumeration(maxPoint, allRoots, true);
      break;
    }
  case eMin:
    {
      GeneratePointEnumeration(minPoint, allRoots, false);
      break;
    }
  case eAny:
    {
      GeneratePointEnumeration(maxPoint, allRoots, true);
      GeneratePointEnumeration(minPoint, allRoots, false);
    }
  }
  return _funcExtremums;
}

void FBFunction::GeneratePointEnumeration(std::vector<std::vector<unsigned int>>& iPointIndexes, std::vector<std::vector<double>>& allRoots, bool iIsMax)
{
  unsigned int totalVariants = 1;

  for(unsigned int i = 0; i < iPointIndexes.size(); i++)
    totalVariants *= iPointIndexes[i].size();

  for(unsigned int idx = 0; idx < totalVariants; idx ++) {
    double* data = new double[iPointIndexes.size()];
    unsigned int currIdx = idx;
    for(unsigned int i = 0; i < iPointIndexes.size(); i++) {
      data[i] = allRoots[i][iPointIndexes[i][currIdx % iPointIndexes[i].size()]];
      currIdx = currIdx / iPointIndexes[i].size();
    }
    _funcExtremums.push_back(new FBFunctionExtremumInfo(new FBNDimPoint(data, iPointIndexes.size()), (iIsMax ? eMax : eMin)));
  }
}

FBFunction* FBFunction::CreateCopy() const
{
  return new FBFunction(_usedBasis, GetVariableCount(), _coefs, _isContainsConstant, _tolerance);
}

FBFunction* FBFunction::CreateSuperpositionFunc(std::vector<FBFunction*> iArgFunctions, const FBRangeRect* ipArgCommonDOM, const FBRangeRect* ipBaseFuncDOM) const
{
  FBAssertV(GetVariableCount() != NULL, NULL);
  FBAssertV(ipArgCommonDOM     != NULL, NULL);
  FBAssertV(GetVariableCount() == iArgFunctions.size(), NULL);
  unsigned int newVarCount = iArgFunctions[0]->GetVariableCount();

  for(unsigned int i = 1; i < iArgFunctions.size(); i++) {
    FBAssertV(newVarCount == iArgFunctions[i]->GetVariableCount(), NULL);
  }
  FBAssertV(ipArgCommonDOM->GetDim() == newVarCount, NULL);

  const double* argMins = ipArgCommonDOM->GetMines();
  const double* argMaxs = ipArgCommonDOM->GetMaxes();

  double* toA = new double[newVarCount];
  double* toB = new double[newVarCount];

  for(unsigned int i = 0 ; i < newVarCount; i++) {
    toA[i] = (argMaxs[i] - argMins[i]) / 2;
    toB[i] = (argMaxs[i] + argMins[i]) / 2;
  }

  unsigned int maxArgDeg = 0;
  std::vector<FBFunction*> polinomialFuncs;
  std::vector<FBFunction*> notNormedPolinomial;
  for(unsigned int i = 0; i < iArgFunctions.size(); i++) {
    FBFunction* pPolinome = iArgFunctions[i]->CreatePolynomialExpansion(ipArgCommonDOM);
    FBAssertV(pPolinome != NULL, NULL);

    FBFunction* pRangedPolinome = pPolinome->EvaluateVariableReplace(toA, toB);

    int deg = static_cast<int> (pRangedPolinome->GetSimplePolinomeBasis()->GetMaxDeg() + TOLERANCE);
    FBAssertV(deg > 0, NULL);
    maxArgDeg = FBMax(maxArgDeg, (unsigned int) deg);

    notNormedPolinomial.push_back(pPolinome);
    polinomialFuncs.push_back(pRangedPolinome);
    //printf("ranged = %s\n", pRangedPolinome->ToString().c_str());
  }

  FBFunction* pBasePolinome = CreatePolynomialExpansion(ipBaseFuncDOM);
  if(pBasePolinome == NULL) {
    double* maxes = new double[GetVariableCount()];
    double* mines = new double[GetVariableCount()];

    for(unsigned int i = 0; i < polinomialFuncs.size(); i++) {
      maxes[i] = - FBBigNumber;
      mines[i] =   FBBigNumber;

      std::vector<FBFunctionExtremumInfo*> extremums = notNormedPolinomial[i]->EvaluateExtremums(ipArgCommonDOM, eAny, NULL);
      for(unsigned int j = 0; j < extremums.size(); j++) {
        double fValue = notNormedPolinomial[i]->EvaluateFunctionValue(*extremums[i]->pPoint);
        FBAssertC(!FBIsNAN(fValue));
        maxes[i] = FBMax(maxes[i], fValue);
        mines[i] = FBMin(mines[i], fValue);
      }
      delete notNormedPolinomial[i];
    }
    ipBaseFuncDOM = new FBRangeRect(mines, maxes, GetVariableCount(), false, _tolerance);
    pBasePolinome = CreatePolynomialExpansion(ipBaseFuncDOM);
    delete ipBaseFuncDOM;
    FBAssertV(pBasePolinome, NULL);
  }

  int bdeg = static_cast<int> (pBasePolinome->GetSimplePolinomeBasis()->GetMaxDeg() + TOLERANCE);
  FBAssertV(bdeg > 0, NULL);
  unsigned int basePolinomeDeg = (unsigned int)bdeg;

  unsigned int newDeg = 4;
  unsigned int newItemCount = newDeg * newVarCount;

  double* righPart = new double[newItemCount];
  for(unsigned int i = 0; i < newItemCount; i++) 
    righPart[i] = 0;

  const FBPolynomialBasis* basePolinomeBasis = pBasePolinome->GetSimplePolinomeBasis();
  const double* pBaseFuncCoefs = pBasePolinome->_coefs.GetPointer();

  int* pMultiplicationIndex = new int[static_cast<int>(basePolinomeBasis->GetMaxDeg() + TOLERANCE) + 1];
  int* pMultiplicationDegs = new int[newVarCount + 1];

  unsigned int basePolinomeCoefIdx = 0;
  for(unsigned int baseVar = 0; baseVar < GetVariableCount(); baseVar++) {
    for(unsigned int baseDegIdx = 0; baseDegIdx < basePolinomeBasis->GetDegs().size(); baseDegIdx++) {
      int deg = static_cast<int>(basePolinomeBasis->GetDegs()[baseDegIdx] + TOLERANCE);
      // iterate on arg func mult coefs

      const double* pCurrArgFuncCoefs = polinomialFuncs[baseVar]->_coefs.GetPointer();
      unsigned int currArgFuncElementPerVariable = polinomialFuncs[baseVar]->_elementPerVariable;
      unsigned int currArgFuncCoefCount = currArgFuncElementPerVariable * polinomialFuncs[baseVar]->_variableNumber;
      currArgFuncCoefCount += polinomialFuncs[baseVar]->_isContainsConstant ? 1 : 0;
      const FBPolynomialBasis* pCurrArgFunc = polinomialFuncs[baseVar]->GetSimplePolinomeBasis();

      for(int i = 0; i < deg + 1; i++)
        pMultiplicationIndex[i] = 0;

      while(true) {
        // get current multiplication
        for(unsigned int i = 0; i < newVarCount + 1; i++)
          pMultiplicationDegs[i] = 0;

        double currentCoef = 1;

        for(int i = 0; i < deg; i++) {
          int currVarIdx = pMultiplicationIndex[i] / currArgFuncElementPerVariable;
          int currVarDeg = static_cast<int>(pCurrArgFunc->GetDegs()[pMultiplicationIndex[i] % currArgFuncElementPerVariable] + TOLERANCE);

          pMultiplicationDegs[currVarIdx] += currVarDeg;
          currentCoef *= pCurrArgFuncCoefs[pMultiplicationIndex[i]];
        }

        // evaluate current item integral value
        double currIntergralMultiplyer = 1;
        unsigned int rightPartIdx = 0;
        for(unsigned int i = 0; i < newVarCount; i++) {
          for(unsigned int j = 1; j <= newDeg; j++) { // deg of right part polinome 
            for(unsigned int legendreCoef = 0; legendreCoef <= j; legendreCoef ++) {
              currIntergralMultiplyer = GetLegendrePolinomCoeficients(j)[legendreCoef];
              if(FBAbs(currIntergralMultiplyer) < TOLERANCE) continue;
              for(unsigned int currCoefDeg = 0; currCoefDeg < newVarCount; currCoefDeg++) {
                unsigned int integralDeg = pMultiplicationDegs[currCoefDeg] + (currCoefDeg == i ? legendreCoef : 0);
                integralDeg = integralDeg + 1;
                currIntergralMultiplyer = currIntergralMultiplyer / integralDeg;
                currIntergralMultiplyer = currIntergralMultiplyer * (FBPow(1.00, integralDeg) - FBPow(-1.00, integralDeg));
              }
              righPart[rightPartIdx] += currIntergralMultiplyer * currentCoef * pBaseFuncCoefs[basePolinomeCoefIdx];
            }
            rightPartIdx++;
          }
        }

        // increment coef iterator
        int currentIncrementIdx = 0;
incTry: pMultiplicationIndex[currentIncrementIdx]++;
        if(pMultiplicationIndex[currentIncrementIdx] == currArgFuncCoefCount) {
          pMultiplicationIndex[currentIncrementIdx] = 0;
          currentIncrementIdx ++;
          if(currentIncrementIdx == deg) break; // iteration done
          goto incTry;
        }
      }
      basePolinomeCoefIdx++;
    }
  }

  delete pMultiplicationIndex;
  delete pMultiplicationDegs;

  for(unsigned int i = 0; i < newItemCount; i++) {
    unsigned int polinomeDeg = (i % newDeg) + 1;
    //printf(" r[%d] = %f \n ", i, righPart[i]);
    righPart[i] *= (2*polinomeDeg + 1)/2;
  }

  std::vector<FBAutoPtr<IFBFunctionBasis>> newBasis;
  newBasis.push_back(new FBLegendreBasis(newDeg));
  FBFunction* pLegendreFunc = new FBFunction(newBasis, newVarCount, righPart, false, _tolerance);
  FBFunction* pPolinomeFunc = pLegendreFunc->CreatePolynomialExpansion(NULL);
  delete pLegendreFunc;

  // back range operation 
  for(unsigned int i = 0 ; i < newVarCount; i++) {
    toA[i] = 2 / (argMaxs[i] - argMins[i]);
    toB[i] = - (argMaxs[i] + argMins[i]) / (argMaxs[i] - argMins[i]);
  }

  FBFunction* pNormedResult = pPolinomeFunc->EvaluateVariableReplace(toA, toB);
  delete pPolinomeFunc;
  delete toA;
  delete toB;

  return pNormedResult;
}

FBFunction* FBFunction::CreatePolynomialExpansion(const FBRangeRect* ipArgCommonDOM) const
{
  if(_usedBasis.size() == 1) {
    const FBPolynomialBasis* pPolinomialBasis = dynamic_cast<const FBPolynomialBasis*>(_usedBasis[0].GetPointer());
    if(pPolinomialBasis != NULL) {
      if(pPolinomialBasis->IsAllDegsInteger()) {
        return CreateCopy();
      }
    } else {
      const FBLegendreBasis* pLegendreBasis = dynamic_cast<const FBLegendreBasis*>(_usedBasis[0].GetPointer());
      if(pLegendreBasis != NULL) {
        unsigned int coefCount = GetVariableCount() * pLegendreBasis->GetMaxDeg() + 1;
        double* coefs = new double[coefCount];
        for(unsigned int i = 0; i < coefCount; i++) {
          coefs[i] = 0;
        }

        for(unsigned int deg = 0; deg < pLegendreBasis->GetMaxDeg(); deg++) {
          for(unsigned int varId = 0; varId < GetVariableCount(); varId++) {
            double coef = _coefs[varId * pLegendreBasis->GetMaxDeg() + deg];

            double* pLegendreCoefs = GetLegendrePolinomCoeficients(deg + 1);
            for(unsigned int polinomeDeg = 0; polinomeDeg < pLegendreBasis->GetMaxDeg(); polinomeDeg++) {
              coefs[varId * pLegendreBasis->GetMaxDeg() + polinomeDeg] += coef * pLegendreCoefs[polinomeDeg + 1];
            }
            coefs[coefCount - 1] += coef * pLegendreCoefs[0];
          }
        }
        return CreateMinimalPolinomialFunction(pLegendreBasis->GetMaxDeg(), coefs);
      }
    }
  }
  if(ipArgCommonDOM == NULL) {
    return NULL;
  }
  return CreatePolynomialExpansionUsingLegendrePolinoms(ipArgCommonDOM);
}

// evaluate function with a1(ax + b) coefs
FBFunction* FBFunction::EvaluateVariableReplace(const double* ipA, const double* ipB) const
{
  const FBPolynomialBasis* pBaseBasis = GetSimplePolinomeBasis();
  if(pBaseBasis == NULL) return NULL;

  unsigned int newElementsPerVar = static_cast<unsigned int> ( TOLERANCE + pBaseBasis->GetMaxDeg());

  double* pNewFuncCoefs = new double[newElementsPerVar * _variableNumber + 1];
  for(unsigned int i = 0; i < newElementsPerVar * _variableNumber + 1; i++)
    pNewFuncCoefs[i] = 0;

  unsigned int coefIdx = 0;
  for(unsigned int varIDx = 0; varIDx < _variableNumber; varIDx++) {
    for(unsigned int degIdx = 0; degIdx < pBaseBasis->GetDegs().size(); degIdx++) {
      unsigned int deg = static_cast<unsigned int> (pBaseBasis->GetDegs()[degIdx] + TOLERANCE);
      for(unsigned int i = 0; i <= deg; i++) {
        double coefValue = _coefs[coefIdx] * FBPow(ipA[varIDx], i) * FBPow(ipB[varIDx], deg - i) * FBBinomial(deg, i);
        if(i == 0) {
          pNewFuncCoefs[newElementsPerVar * _variableNumber] += coefValue;
        } else {
          pNewFuncCoefs[varIDx * newElementsPerVar + i - 1] += coefValue;
        }
      }
      coefIdx++;
    }
  }

  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  basis.push_back(new FBPolynomialBasis(newElementsPerVar));
  return new FBFunction(basis, _variableNumber, pNewFuncCoefs, true, _tolerance);
}

//http://gameford.ru/avtomaticheskie-cifrovye-mashiny/453-polinomialnye-razlozheniya.html
#define LEGENDRE_EXPANSION_INTEGRATION_POINTS 500
FBFunction* FBFunction::CreatePolynomialExpansionUsingLegendrePolinoms(const FBRangeRect* ipArgCommonDOM) const
{
  FBAssertV(ipArgCommonDOM, NULL);

  const double* pDomMax = ipArgCommonDOM->GetMaxes();
  const double* pDomMin = ipArgCommonDOM->GetMines();

  double* pFuncArg = new double[GetVariableCount()];
  for(unsigned int i = 0; i < GetVariableCount(); i++) {
    pFuncArg[i] = 0;
  }

  double* pNewData = new double[GetVariableCount() * 4 + 1];
  pNewData[GetVariableCount() * 4] = 0;

  unsigned int newDataPc = 0;

  for(unsigned int varIdx = 0; varIdx < GetVariableCount(); varIdx++) {
    double a[5];
    for(unsigned int coef = 0; coef < 5; coef ++) {
      a[coef] = 0;

      double prValue = 0;
      for(unsigned int integrationPoint = 0; integrationPoint <= LEGENDRE_EXPANSION_INTEGRATION_POINTS; integrationPoint++) {
        double legendreArgValue = ((double) integrationPoint) * 2.00 / LEGENDRE_EXPANSION_INTEGRATION_POINTS - 1.00;
        pFuncArg[varIdx] = pDomMin[varIdx] + ((double) integrationPoint / LEGENDRE_EXPANSION_INTEGRATION_POINTS) * (pDomMax[varIdx] - pDomMin[varIdx]);
        double currValue;
        FBAssertV(EvaluateFunctionValue(pFuncArg, currValue, varIdx), NULL);
        currValue *= EvaluateLegendrePolinomValue(legendreArgValue, coef);

        if(integrationPoint != 0) {
          a[coef] += (prValue + currValue) / 2;
        }
        prValue = currValue;
      }
      a[coef] = a[coef] * ( 2.0 * coef + 1.0) / 2 ;
      a[coef] = a[coef] * ( 2.0 / LEGENDRE_EXPANSION_INTEGRATION_POINTS);
    }
    pFuncArg[varIdx] = 0;
    
    double c[5];
    c[0] = a[1] - (3.0*a[3])/2;
    c[1] = 3.0*a[2]/2 - (15.0*a[4])/4;
    c[2] = (5.0*a[3])/2;
    c[3] = (35.0*a[4])/8;
    c[4] = a[0] - a[2]/2 + (3.0*a[4])/8;

    // polynomial c[4] + c[0]*x + c[1]*x^2 + c[2]*x^3 + c[4]*x^4 in DOM [-1; 1]
    // move it to native DOM by variable replace vra*x + vrb 

    double vra = - 2.0 /(pDomMin[varIdx] - pDomMax[varIdx]);
    double vrb = (pDomMax[varIdx] + pDomMin[varIdx]) / (pDomMin[varIdx] - pDomMax[varIdx]);

    pNewData[newDataPc + 3] = FBPow(vra, 4) * c[3];
    pNewData[newDataPc + 2] = FBPow(vra, 3) * (c[2] + 4*c[3]*vrb);
    pNewData[newDataPc + 1] = FBPow(vra, 2) * (c[1] + 3*c[2]*vrb + 6*c[3]*FBPow(vrb,2));
    pNewData[newDataPc + 0] = FBPow(vra, 1) * (c[0] + 2*c[1]*vrb + 3*c[2]*FBPow(vrb,2) + 4*c[3]*FBPow(vrb,3));
    pNewData[GetVariableCount() * 4] += c[4] + c[0]*vrb + c[1]*FBPow(vrb, 2) + c[2]*FBPow(vrb, 3) + c[3]*FBPow(vrb, 4);

    newDataPc += 4;
  }
  delete pFuncArg;
  return CreateMinimalPolinomialFunction(4, pNewData);
}

FBFunction* FBFunction::CreateMinimalPolinomialFunction(unsigned int iBaseDeg, double* cipData) const
{
  // evaluate average coefficient
  double coefTolerance = 0;
  for(unsigned int i = 0; i < iBaseDeg * GetVariableCount(); i++) {
    coefTolerance += cipData[i];
  }
  coefTolerance = coefTolerance / (iBaseDeg * GetVariableCount());
  coefTolerance = coefTolerance / 100; // 1% of average coef

  unsigned int minimalDeg = iBaseDeg;
  bool _containsNonZero = false;

  while(minimalDeg > 0 && !_containsNonZero) {
    for(unsigned int varIdx = 0; varIdx < GetVariableCount(); varIdx ++) {
      if(FBAbs(cipData[varIdx * iBaseDeg + minimalDeg - 1]) > coefTolerance) {
        _containsNonZero = true;
        break;
      }
    }
    if(!_containsNonZero)
      minimalDeg--;
  }

  if(minimalDeg == iBaseDeg) {
     std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
     basis.push_back(new FBPolynomialBasis(iBaseDeg));
     return new FBFunction(basis, GetVariableCount(), cipData, true, _tolerance);
  } else {
    double* pNewData = new double[GetVariableCount() * minimalDeg + 1];

    for(unsigned int i = 0; i < minimalDeg; i++) {
      for(unsigned int j = 0; j < GetVariableCount(); j++) {
        pNewData[i + j * minimalDeg] = cipData[i + j * iBaseDeg];
      }
    }
    pNewData[GetVariableCount() * minimalDeg] = cipData[iBaseDeg * GetVariableCount()];
    delete cipData;

    std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
    basis.push_back(new FBPolynomialBasis(minimalDeg));
    bool isContainsConstant = FBAbs(pNewData[GetVariableCount() * minimalDeg]) > coefTolerance || minimalDeg == 0;

    return new FBFunction(basis, GetVariableCount(), pNewData, isContainsConstant, _tolerance);
  }
}
