// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Implements storage shell fo function
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBFunction_h
#define FBFunction_h

#include "FBMathUtils.h"
#include "FBNullableType.h"
#include "FBAutoPtr.h"

#include "FBFunctionValue.h"

class IFBFunctionBasis;
class FBPolynomialBasis;

class FBFunctionBuilder;
class FBNDimPoint;
class FBRangeRect;

enum FBFunctionExtremumType
{
  eMax,
  eMin,
  eAny
};

struct FBFunctionExtremumInfo
{
  FBFunctionExtremumInfo(FBNDimPoint* cipPoint, FBFunctionExtremumType iIsMax);
  ~FBFunctionExtremumInfo();

  const FBNDimPoint* pPoint;
  const FBFunctionExtremumType type;
};

class FBFunction
{
  friend class FBFunctionBuilder;
public:
  FBFunction(const std::vector<FBAutoPtr<IFBFunctionBasis>>& ipBasis, unsigned int iVarCount, FBAutoPtr<const double> ipCoefs, bool iIsContainsConstant = false, double iTolerance = TOLERANCE);
  ~FBFunction();

  std::vector<FBFunctionExtremumInfo*>& EvaluateExtremums(const FBRangeRect* ipFunctionDOM, FBFunctionExtremumType iType, std::vector<FBNullableType<double>>* ipFixedValues);

  // works only for function with one variable
  std::vector<double> EvaluateRoots(double iRangeMin, double iRangeMax) const;
  std::vector<double> EvaluateRootsByBisectionMethod(double iRangeMin, double iRangeMax) const;

  FBFunction* CreatePolynomialExpansion(const FBRangeRect* ipArgCommonDOM) const; // call Legendre generator if unable convert 
  FBFunction* CreatePolynomialExpansionUsingLegendrePolinoms(const FBRangeRect* ipArgCommonDOM) const;

  FBFunction* CreateSuperpositionFunc(std::vector<FBFunction*> iArgFunctions, const FBRangeRect* ipArgCommonDOM, const FBRangeRect* ipBaseFuncDOM = NULL) const;
  FBFunction* CreateCopy() const;

  bool EvaluateFunctionValue(FBFunctionPointData& iArgRes, int iElevateOnlyByVariable = -1) const;
  bool EvaluateFunctionValue(const FBFunctionArgument& ipArgument, double& oFuncValue, int iElevateOnlyByVariable = -1) const;
  double EvaluateFunctionValue(const FBFunctionArgument& ipArgument) const;

  bool EvaluateFunctionValue(const double* ipArgument, double& oFuncValue, int iElevateOnlyByVariable = -1) const;
  double EvaluateFunctionValue(const double* ipArgument) const;

  bool EvaluateFunctionValue(FBAutoPtr<const double> ipArgument, double& oFuncValue, int iElevateOnlyByVariable = -1) const;
  double EvaluateFunctionValue(FBAutoPtr<const double> ipArgument) const;

  bool EvaluateFunctionValue(FBAutoPtr<double> ipArgument, double& oFuncValue, int iElevateOnlyByVariable = -1) const;
  double EvaluateFunctionValue(FBAutoPtr<double> ipArgument) const;

  FBFunction* EvaluateDirivaryFunction(unsigned int iVarIdx);

  unsigned int GetVariableCount() const { return _variableNumber; }
  bool IsUsedVariable(unsigned int iVarIdx) const;

  // in case func is not polynomial return NULL;
  const FBPolynomialBasis* GetSimplePolinomeBasis() const;
  // evaluate function with a1(ax + b) coefs. Works only for simple polynomial
  FBFunction* EvaluateVariableReplace(const double* ipA, const double* ipB) const;

  std::string ToString() const;

  const double* GetRawCoefs() const { return _coefs.GetPointer(); }

private:
  // reduce deg if zero coefs contains
  FBFunction* CreateMinimalPolinomialFunction(unsigned int iBaseDeg, double* cipData) const;

  const double _tolerance;
  const bool _isContainsConstant;
  std::vector<FBFunctionExtremumInfo*> _funcExtremums;
  void GeneratePointEnumeration(std::vector<std::vector<unsigned int>>& iPointIndexes, std::vector<std::vector<double>>& allRoots, bool iIsMax);

  void ResetDerivatyFuncValues();

  const std::vector<FBAutoPtr<IFBFunctionBasis>> _usedBasis;
  const unsigned int _variableNumber;
  unsigned int _elementPerVariable;

  FBFunction** _derivates;

  FBAutoPtr<const double> _coefs;
};

#endif // FBFunction_h
