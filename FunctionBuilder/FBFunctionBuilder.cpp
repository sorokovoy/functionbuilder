// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Build function from points as decomposition on specified basis
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBFunctionBuilder.h"
#include "FBFunction.h"
#include "FBMathUtils.h"

FBFunctionBuilder::FBFunctionBuilder(std::vector<FBAutoPtr<IFBFunctionBasis>>& cipBasisFuncs, unsigned int iVariableNumber, double iTolerance, bool iIsContainsConstanct, int iAverageCapasity)
: _variableCount(iVariableNumber),
  _destrVarsPerVariable(GetElementsPerVariable(cipBasisFuncs)),
 _isContainsConstant(iIsContainsConstanct || _destrVarsPerVariable == 0), // for constant function
 _tolerance(iTolerance)
{
  _processedApproximationPoints = 0;
  _usedBasis = cipBasisFuncs;

  _matrixSize = _destrVarsPerVariable * _variableCount + (_isContainsConstant ? 1 : 0);

  _validateSampleSetSize = GetRequirePointCount() * 3;

  _lenghtOfAverage = iAverageCapasity < 0 ? _matrixSize * 2 : FBMax((unsigned int)iAverageCapasity, _matrixSize + 1 + (_isContainsConstant ? 1 : 0));

  for (unsigned int i = 0; i < _matrixSize; i++) {
    std::vector<FBFixedSizeAverageCalculator<double>*> curr;
    for (unsigned int j = 0; j < _matrixSize + 1; j++) {
      curr.push_back(new FBFixedSizeAverageCalculator<double>(_lenghtOfAverage));
    }
    _matrixSource.push_back(curr);
  }
  
  _correlationMatrix = new Eigen::MatrixXd(_matrixSize, _matrixSize);
  _correlationRightPart = new Eigen::VectorXd(_matrixSize);

  _solutionData = NULL;
  _pFunction = NULL;
  _isRedy = false;
  _haveSomeSolution = false;
  _coefRecalculateRequired = true;
}

FBFunctionBuilder::~FBFunctionBuilder()
{
  for (unsigned int i = 0; i < _matrixSize; i++)
    for (unsigned int j = 0; j < _matrixSize + 1; j++)
      delete _matrixSource[i][j];
  _normedData.clear();
  delete _pFunction;
}

unsigned int FBFunctionBuilder::GetElementsPerVariable(std::vector<FBAutoPtr<IFBFunctionBasis>>& iBasisFuncs)
{
  unsigned int destrVarsPerVariable = 0;
  for(unsigned int i = 0; i < iBasisFuncs.size(); i++)
    destrVarsPerVariable += iBasisFuncs[i]->GetVarCountPerElement();
  return destrVarsPerVariable;
}

bool FBFunctionBuilder::AddApproximationPoint(double* cipCondition)
{
  return AddApproximationPoint(FBFunctionPointData(FBNDimPoint(cipCondition, _variableCount), cipCondition[_variableCount]));
}

bool FBFunctionBuilder::AddApproximationPoint(double* cipCondition, double iResult)
{
  return AddApproximationPoint(FBFunctionPointData(FBNDimPoint(cipCondition, _variableCount), iResult));
}

bool FBFunctionBuilder::AddApproximationPoint(const FBFunctionPointData& iPoint)
{
  if(_variableCount == 0) return false;
  _normedData.push_back(iPoint);
  if(_normedData.size() > _validateSampleSetSize) {
    _normedData.pop_front();
  }

  return AddNormedApproximationPoint(iPoint);
}

bool FBFunctionBuilder::AddNormedApproximationPoint(const FBFunctionPointData& iCondition)
{
  int i = 0;
  int j = 0;

  double iResult = iCondition.fVal;

  if(_pFunction != NULL && !_coefRecalculateRequired) {
    double fValue;
    // if we found good function no any work doing;
    if(_pFunction->EvaluateFunctionValue(iCondition, fValue) && FBEqual(fValue, iResult, _tolerance)) {
      return false;
    }
  }

  unsigned int IBlockIdx = 0;
  unsigned int IElementIdx = 0;
  for (unsigned int IVar = 0; IVar < _variableCount; IVar++) {
    double IVariableValue = iCondition.arg[IBlockIdx].GetData()[IElementIdx];
    IElementIdx++;
    if(IElementIdx == iCondition.arg[IBlockIdx].GetDim() && iCondition.arg.size() != 1) {
      IElementIdx = 0;
      IBlockIdx++;
    }

    for (unsigned int IdestId = 0; IdestId < _usedBasis.size(); IdestId++) {
      IFBFunctionBasis* IDest = _usedBasis[IdestId];
      for (unsigned int IcurrDestVarId = 0; IcurrDestVarId < IDest->GetVarCountPerElement(); IcurrDestVarId++) {
        j = 0;
        double iValue = IDest->CalculateValueByIdx(IVariableValue, IcurrDestVarId);
        unsigned int JBlockIdx = 0;
        unsigned int JElementIdx = 0;
        for (unsigned int JVar = 0; JVar < _variableCount; JVar++) {
          double JVariableValue = iCondition.arg[JBlockIdx].GetData()[JElementIdx];
          JElementIdx++;
          if(JElementIdx == iCondition.arg[JBlockIdx].GetDim() && iCondition.arg.size() != 1) {
            JElementIdx = 0;
            JBlockIdx++;
          }

          for (unsigned int JdestId = 0; JdestId < _usedBasis.size(); JdestId++) {
            IFBFunctionBasis* JDest = _usedBasis[JdestId];
            for (unsigned int JcurrDestVarId = 0; JcurrDestVarId < JDest->GetVarCountPerElement(); JcurrDestVarId++) {
              double jValue = JDest->CalculateValueByIdx(JVariableValue, JcurrDestVarId);
              _matrixSource[i][j]->AddElement(iValue * jValue);
              (*_correlationMatrix)(i, j) = _matrixSource[i][j]->GetAverage();
              j++;
            }
          }
        }
        _matrixSource[i][_matrixSize]->AddElement(iValue * iResult);
        (*_correlationRightPart)(i) = _matrixSource[i][_matrixSize]->GetAverage();

        if(_isContainsConstant) {
          _matrixSource[i][_matrixSize-1]->AddElement(iValue); // for last column jValue = 1
          (*_correlationMatrix)(i, _matrixSize-1) = _matrixSource[i][_matrixSize-1]->GetAverage();
        }

        i++;
      }
    }
  }

  if(_isContainsConstant) {
    // for last row (constant) iValue = 1
    j = 0;
    unsigned int JBlockIdx = 0;
    unsigned int JElementIdx = 0;
    for (unsigned int JVar = 0; JVar < _variableCount; JVar++) {
      double JVariableValue = iCondition.arg[JBlockIdx].GetData()[JElementIdx];
      JElementIdx++;
      if(JElementIdx == iCondition.arg[JBlockIdx].GetDim() && iCondition.arg.size() != 1) {
        JElementIdx = 0;
        JBlockIdx++;
      }

      for (unsigned int JdestId = 0; JdestId < _usedBasis.size(); JdestId++) {
        IFBFunctionBasis* JDest = _usedBasis[JdestId];
        for (unsigned int JcurrDestVarId = 0; JcurrDestVarId < JDest->GetVarCountPerElement(); JcurrDestVarId++) {
          double jValue = JDest->CalculateValueByIdx(JVariableValue, JcurrDestVarId);
          _matrixSource[_matrixSize-1][j]->AddElement(jValue);
          (*_correlationMatrix)(_matrixSize-1, j) = _matrixSource[_matrixSize-1][j]->GetAverage();
          j++;
        }
      }
    }
    (*_correlationMatrix)(_matrixSize-1, _matrixSize-1) = 1;
    _matrixSource[_matrixSize-1][_matrixSize]->AddElement(iResult);
    (*_correlationRightPart)(_matrixSize-1) = _matrixSource[_matrixSize-1][_matrixSize]->GetAverage();
  }


  _processedApproximationPoints ++;
  if(_processedApproximationPoints >= GetRequirePointCount())
    _isRedy = true;

  _coefRecalculateRequired = true;
  return true;
}

#include <iostream>
#include "Eigen/LU"
#include "Eigen/SVD"

FBFunction* FBFunctionBuilder::EvaluateFunction(double iErrorTolerance)
{
  if(_variableCount == 0) return NULL;
  if(!_isRedy) return NULL;
  if(iErrorTolerance < 0) iErrorTolerance = _tolerance;

  if(!_coefRecalculateRequired && _pFunction != NULL)
    return _pFunction;

  if(_solutionData == NULL) {
    _solutionData = new double[_matrixSize];
    _pFunction = new FBFunction(_usedBasis, _variableCount, _solutionData.GetConstInstance(), _isContainsConstant, _tolerance);
  }

  //_solution = (*_correlationMatrix).lu().solve(*_correlationRightPart);

  // use SVD for overlap linear dependency
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(*_correlationMatrix, Eigen::ComputeThinU | Eigen::ComputeThinV);
  _solution = svd.solve(*_correlationRightPart);

  // std::cout << *_correlationMatrix << std::endl;
  // std::cout << *_correlationRightPart << std::endl;
  // std::cout << _solution << std::endl;

  double tmp = 0;
  for(unsigned int i = 0; i < _matrixSize; i++) {
    _solutionData[i] = _solution[i];
    tmp += _solutionData[i];
  }

  if(FBIsNAN(tmp) || (CalculateAverageError() / _normedData.size() > iErrorTolerance)) {
    _haveSomeSolution = false;
    _pFunction->ResetDerivatyFuncValues();
    return NULL;
  }

  _coefRecalculateRequired = false;

  if(_haveSomeSolution) {
    _previousSolution -= _solution;
    if(FBLess(_tolerance * _variableCount, _previousSolution.norm()))
      _pFunction->ResetDerivatyFuncValues();
  }
  _haveSomeSolution = true;
  _previousSolution = _solution;
  return _pFunction;
}

double FBFunctionBuilder::CalculateAverageError()
{
  if(_pFunction == NULL)
    EvaluateFunction();

  if(_pFunction == NULL)
    return FBBigNumber;

  double error = 0;
  unsigned int elementCount = 0;
  for(std::list<FBFunctionPointData>::iterator it = _normedData.begin(); it != _normedData.end(); it++) {
    double res = 0;
    FBAssertC(_pFunction->EvaluateFunctionValue(it->arg, res));

    error += FBSqr(it->fVal - res);
    elementCount++;
  }

  //std::cout << "func " << _pFunction->ToString() << std::endl;
  return elementCount == 0 ? 0 : FBSqrt(error / elementCount);
}

