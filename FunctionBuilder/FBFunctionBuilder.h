// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Build function from points as decomposition on specified basis
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBFunctionBuilder_h
#define FBFunctionBuilder_h

#include "IFBFunctionBasis.h"

#include "FBPrecompiled.h"

#include "FBAutoPtr.h"
#include "FBMathUtils.h"
#include "FBFixedSizeAverageCalculator.h"
#include "FBFunctionValue.h"

// using linear algebra engine
#include "Eigen/Core"

class FBFunction;

class FBFunctionBuilder
{
public:
  static unsigned int GetElementsPerVariable(std::vector<FBAutoPtr<IFBFunctionBasis>>& iBasisFuncs);

  FBFunctionBuilder(std::vector<FBAutoPtr<IFBFunctionBasis>>& cipBasisFuncs, unsigned int iVariableNumber, double iTolerance = TOLERANCE, bool iIsContainsConstanct = false, int iAverageCapasity = -1);
  ~FBFunctionBuilder();

  // array of iVariableNumber + 1 size. First iVariableNumber of this is function arguments last is function value
  bool AddApproximationPoint(double* cipArgRes);
  bool AddApproximationPoint(double* cipCondition, double iResult);
  bool AddApproximationPoint(const FBFunctionPointData& iPoint); // return true if evaluated function update;

  FBFunction* EvaluateFunction(double iErrorTolerance = -1);

  std::string ToString() const;
  bool IsRedy() const { return _isRedy; }

  unsigned int GetRequirePointCount() const { return _matrixSize; }
  unsigned int GetContainedPointCount() const { return _normedData.size(); }
  unsigned int GetVariableCount() const { return _variableCount; }
  unsigned int GetValidateSampleSetSize() const { return _validateSampleSetSize; }

  const std::list<FBFunctionPointData>& GetData() const { return _normedData; }

private:
  double CalculateAverageError(); // calculate average error on sample set
  unsigned int _validateSampleSetSize;

  const unsigned int _destrVarsPerVariable;
  const unsigned int _variableCount;
  const bool _isContainsConstant;
  const double _tolerance;

  unsigned int _lenghtOfAverage;
  unsigned int _matrixSize;
  unsigned int _processedApproximationPoints;

  bool _isRedy;
  bool _coefRecalculateRequired;

  std::vector<FBAutoPtr<IFBFunctionBasis>> _usedBasis;

  Eigen::MatrixXd* _correlationMatrix;
  Eigen::VectorXd* _correlationRightPart;
  Eigen::VectorXd _solution;
  Eigen::VectorXd _previousSolution;
  bool _haveSomeSolution;

  std::vector<std::vector<FBFixedSizeAverageCalculator<double>*>> _matrixSource;

  std::list<FBFunctionPointData> _normedData;

  FBAutoPtr<double> _solutionData;

  double* _lastData;

  FBFunction* _pFunction;

  int _processedValues;

  bool AddNormedApproximationPoint(const FBFunctionPointData& iValue);
};

#endif // FBFunctionBuilder_h
