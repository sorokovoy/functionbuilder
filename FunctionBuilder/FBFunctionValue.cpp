// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// define function value types
//
//===================================================================
//  MAY 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBFunctionValue.h"

FBFunctionArgument::FBFunctionArgument(const std::vector<FBNDimPoint>& iArg)
:arg(iArg)
{}

FBFunctionArgument::FBFunctionArgument(const FBNDimPoint& iArg)
{
  arg.push_back(iArg);
}

FBFunctionArgument::FBFunctionArgument(const double* ipVal)
{
  arg.push_back(FBNDimPoint(ipVal, 1));
}

FBFunctionArgument::FBFunctionArgument(FBAutoPtr<double> ipVal)
{
  arg.push_back(FBNDimPoint(ipVal.GetConstInstance(), 1));
}

FBFunctionArgument::FBFunctionArgument(FBAutoPtr<const double> ipVal)
{
  arg.push_back(FBNDimPoint(ipVal, 1));
}

FBFunctionPointData::FBFunctionPointData(const std::vector<FBNDimPoint>& iArg, double iFVal) 
:FBFunctionArgument(iArg), fVal(iFVal)
{};

FBFunctionPointData::FBFunctionPointData(const FBNDimPoint& iArg, double iFVal)
:FBFunctionArgument(iArg), fVal(iFVal)
{}