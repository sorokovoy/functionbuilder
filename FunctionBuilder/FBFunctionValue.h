// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// define function value types
//
//===================================================================
//  MAY 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBFunctionValue_H
#define FBFunctionValue_H

#include "FBNDimPoint.h"

struct FBFunctionArgument
{
  FBFunctionArgument() {};
  FBFunctionArgument(const double* cipVal);
  FBFunctionArgument(FBAutoPtr<double> ipVal);
  FBFunctionArgument(FBAutoPtr<const double> ipVal);
  FBFunctionArgument(const std::vector<FBNDimPoint>& iArg);
  FBFunctionArgument(const FBNDimPoint& iArg);
  std::vector<FBNDimPoint> arg;
};

struct FBFunctionPointData : FBFunctionArgument
{
  FBFunctionPointData() { fVal = 0; }
  FBFunctionPointData(const std::vector<FBNDimPoint>& iArg, double iFVal);
  FBFunctionPointData(const FBNDimPoint& iArg, double iFVal);
  double fVal;
};

#endif // FBFunction_h
