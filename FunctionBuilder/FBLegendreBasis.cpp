// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Basis contains from legendre functions 
// http://en.wikipedia.org/wiki/Legendre_polynomials
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBLegendreBasis.h"
#include "FBPolynomialBasis.h"
#include "FBMathUtils.h"

FBLegendreBasis::FBLegendreBasis(unsigned int iDeg)
:_deg(FBMin(FBMax(iDeg, 1), 10))
{}

unsigned int FBLegendreBasis::GetVarCountPerElement() const
{
  return _deg;
}

double FBLegendreBasis::CalculateValueByIdx(double iValue, int idx) const
{
  return EvaluateLegendrePolinomValue(iValue, idx + 1);
}

IFBFunctionBasis* FBLegendreBasis::CreateDerivaty() const
{
  return new FBPolynomialBasis(_deg);
}

double FBLegendreBasis::TransformDerivatyCoeficients(double* ioCoefs) const
{
  double* pNewCoefs = new double[_deg];
  for(unsigned int i = 0; i < _deg; i++) {
    pNewCoefs[i] = 0;
  }
  double constant = 0;

  for(unsigned int d = 0; d < _deg; d++) {
    double* LCoef = GetLegendrePolinomCoeficients(d + 1);
    constant += ioCoefs[d] * LCoef[1];
    for(unsigned int coef = 2; coef < d + 2; coef++) {
      pNewCoefs[coef - 2] += ioCoefs[d] * LCoef[coef] * coef;
    }
  }
  memcpy(ioCoefs, pNewCoefs, sizeof(double) * _deg);
  delete pNewCoefs;
  return constant;
}

IFBFunctionBasis* FBLegendreBasis::CreateCopy() const
{
  return new FBLegendreBasis(_deg);
}

std::string FBLegendreBasis::ShowDependencyType(int idx, const char* varName) const
{
  char buf[255];
  sprintf_s(buf, 255, "LegendreP[%d, %s]", idx + 1, varName);
  return buf;
}

