// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Basis contains from legendre functions 
// http://en.wikipedia.org/wiki/Legendre_polynomials
// Deg of basis starts from 1. it equal to basis {x}
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBLegendreBasis_H
#define FBLegendreBasis_H

#include "IFBFunctionBasis.h"

class FBLegendreBasis : public IFBFunctionBasis
{
public:
  FBLegendreBasis(unsigned int iDeg);
  unsigned int GetMaxDeg() const { return _deg; }
  virtual unsigned int GetVarCountPerElement() const;
  virtual double CalculateValueByIdx(double iValue, int idx) const;
  virtual IFBFunctionBasis* CreateDerivaty() const;
  virtual IFBFunctionBasis* CreateCopy() const;
  virtual double TransformDerivatyCoeficients(double* ioCoefs) const;
  virtual std::string ShowDependencyType(int idx, const char* varName) const;
private:
  const unsigned int _deg;
};


#endif // FBLegendreBasis_H
