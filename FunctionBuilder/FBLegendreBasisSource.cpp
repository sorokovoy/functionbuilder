// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Create basis contains only from legendre functions
//
//===================================================================
//  APR 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBLegendreBasisSource.h"
#include "FBLegendreBasis.h"
#include "FBConstantBasis.h"

FBLegendreBasisSource::FBLegendreBasisSource()
{
  _currentDeg = 1;
}

std::vector<FBAutoPtr<IFBFunctionBasis>> FBLegendreBasisSource::CreateBasis() const
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> result;
  if(_currentDeg != 1) {
    result.push_back(new FBLegendreBasis(_currentDeg / 2));
  } else {
    result.push_back(new FBConstantBasis());
  }
  return result;
}

bool FBLegendreBasisSource::IsFunctionHasConstant() const
{
  return _currentDeg % 2 == 1;
}

void FBLegendreBasisSource::IncrementDeg()
{
  _currentDeg++;
}

bool FBLegendreBasisSource::IsDegIncrementRequired(unsigned int iCurrPointCount, unsigned int iRequirePointCount, unsigned int iMaxPointCount) const
{
  return iCurrPointCount > (iMaxPointCount + iRequirePointCount) / 2;
}

IFBFunctionBasisSource* FBLegendreBasisSource::CreateCopy() const
{
  FBLegendreBasisSource* pClone = new FBLegendreBasisSource();
  pClone->_currentDeg = _currentDeg;
  return pClone;
}
