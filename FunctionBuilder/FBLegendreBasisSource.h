// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Create basis contains only from legendre functions
//
//===================================================================
//  APR 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBLegendreBasisSource_h
#define FBLegendreBasisSource_h

#include "IFBFunctionBasisSource.h"

class FBLegendreBasisSource : public IFBFunctionBasisSource
{
public:
  FBLegendreBasisSource();

  virtual std::vector<FBAutoPtr<IFBFunctionBasis>> CreateBasis() const;
  virtual bool IsFunctionHasConstant() const;

  virtual void IncrementDeg();
  virtual bool IsDegIncrementRequired(unsigned int iCurrPointCount, unsigned int iRequirePointCount, unsigned int iMaxPointCount) const;

  virtual IFBFunctionBasisSource* CreateCopy() const;

private:
  unsigned int _currentDeg;
};

#endif // IFBFunctionBasisSource_h
