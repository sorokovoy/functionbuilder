// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Math utilities
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================
#include "FBMathUtils.h"
#include <vector>

int FBHash(const char* str)
{
  int h = 0;
  while (*str)
    h = h << 1 ^ *str++;
  return h;
}

double FBPow(double x, unsigned int n)
{
  double r = 1;
  while(n) {
    if(n&1) r*=x;
    n >>= 1;
    x=x*x;
  }
  return r;
}

double FBElevatedFactorialTable[11] = { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800 };
double FBFactorial(unsigned int n)
{
  if(n < 11) {
    return FBElevatedFactorialTable[n];
  }
  double result = n;
  double result_next;
  unsigned int pc = n;
  do {
    result_next = result*(pc-1);
    result = result_next;
    pc--;
  } while(pc>2);
  return result;
}


//------------------------------------------------------------------------------------------------
#define LGSEP_EVAL_FOURTH_DEGREE_POLYNOMIAL(coeff, x) \
  coeff[0]*x*x*x*x + coeff[1]*x*x*x + coeff[2]*x*x + coeff[3]*x + coeff[4]
#define LGSEP_BISECTION_MAX_ITER 500
//------------------------------------------------------------------------------------------------
inline double CubicRoot(double d) {
  return (d < 0 ? -pow(-d, 1/3.0) : pow(d, 1/3.0));
}
//------------------------------------------------------------------------------------------------
unsigned FBSolveFirstDegreePolynomial(double* iCoeffs, double* oRoots, double iTolerance)
{
  //a*x + b = 0
  double a = iCoeffs[0];
  double b = iCoeffs[1];
  if (fabs(a) < iTolerance)
    return 0;
  oRoots[0] = -b/a;
  return 1;
}
//------------------------------------------------------------------------------------------------
unsigned FBSolveSecondDegreePolynomial(double* iCoeffs, double* oRoots, double iTolerance)
{
  //a*x^2 + b*x + c = 0
  double a = iCoeffs[0];
  double b = iCoeffs[1];
  double c = iCoeffs[2];
  if (fabs(a) < iTolerance)
    return FBSolveFirstDegreePolynomial(iCoeffs + 1, oRoots, iTolerance);
  double D = b*b - 4*a*c;
  double a2 = 2*a;
  if (D < -iTolerance)
    return 0;
  if (D < iTolerance) {
    oRoots[0] = -b/a2;
    return 1;
  }
  double l = -b/a2;
  double r = sqrt(D)/a2;
  oRoots[0] = l + r;
  oRoots[1] = l - r;
  return 2;
}
//------------------------------------------------------------------------------------------------
unsigned FBSolveThirdDegreePolynomial(double* iCoeffs, double* oRoots, double iTolerance)
{
  //a*x^3 + b*x^2 + c*x + d = 0
  double  a = iCoeffs[0],
    b = iCoeffs[1],
    c = iCoeffs[2],
    d = iCoeffs[3];
  double roots[3];
  if (fabs(a) < iTolerance)
    return FBSolveSecondDegreePolynomial(iCoeffs + 1, oRoots, iTolerance);
  double F = (3*c/a  - b*b/(a*a))/3,
    G = (2*(b/a)*(b/a)*(b/a) - 9*(b/a)*(c/a) + 27*d/a)/27,
    H = G*G/4 + F*F*F/27;
  if (F == 0 && G == 0){//there are 3 multiple roots
    oRoots[0] = - b/(3*a);
    oRoots[1] = oRoots[0];
    oRoots[2] = oRoots[0];
    return 3;
  }
  if (H <= 0) {  // all roots are real
    double I = sqrt(G*G/4 - H),
      J = CubicRoot(I),
      K = acos(- G/(2*I)),
      L = -J,
      M = cos(K/3),
      N = sqrt(3.0)*sin(K/3),
      P = -b/(3*a);
    roots[0] = 2*J*M + P;
    roots[1] = L*(M + N) + P;
    roots[2] = L*(M - N) + P;
    if (roots[0] <= roots[1] && roots[0] <= roots[2]) {
      oRoots[0] = roots[0];
      if (roots[1] <= roots[2]) {
        oRoots[1] = roots[1];
        oRoots[2] = roots[2];
      } else {
        oRoots[1] = roots[2];
        oRoots[2] = roots[1];
      }
    }
    if (roots[1] <= roots[2] && roots[1] <= roots[0]) {
      oRoots[0] = roots[1];
      if (roots[2] <= roots[0]) {
        oRoots[1] = roots[2];
        oRoots[2] = roots[0];
      } else {
        oRoots[1] = roots[0];
        oRoots[2] = roots[2];
      }
    }
    if (roots[2] <= roots[0] && roots[2] <= roots[1]) {
      oRoots[0] = roots[2];
      if (roots[0] <= roots[1]) {
        oRoots[1] = roots[0];
        oRoots[2] = roots[1];
      } else {
        oRoots[1] = roots[1];
        oRoots[2] = roots[0];
      }
    }
    return 3;
  } else {
    double R = -G/2 + sqrt(H),
      S = CubicRoot(R),
      U = -G/2 - sqrt(H),
      V = CubicRoot(U);
    oRoots[0] = (S + V) - b/(3*a);
    return 1;
  }
  return 0;
}
//------------------------------------------------------------------------------------------------
static unsigned RootFindFourthDegreePolynomial(
  double   *iCoeffs,
  double   iLBound,
  double   iRBound,
  double&  oRoot )
{
    double   a  = iLBound, b = iRBound, c = 0,
      fa = LGSEP_EVAL_FOURTH_DEGREE_POLYNOMIAL(iCoeffs, a),
      fb = LGSEP_EVAL_FOURTH_DEGREE_POLYNOMIAL(iCoeffs, b),
      fc = 0.0;
    if ((fa > 0 && fb > 0) ||
      (fa < 0 && fb < 0) )
      return 1;
    unsigned iter = 0/*, stop = 0*/;
    while ( ++iter < LGSEP_BISECTION_MAX_ITER ) {
      c = a / 2 + b / 2;
      if (!( a < c && c < b))
        break;
      fc = LGSEP_EVAL_FOURTH_DEGREE_POLYNOMIAL(iCoeffs, c);
      if ( ( fa >= 0 && fc <= 0) || (fa <= 0 && fc >= 0)) {
        b  = c;
        fb = fc;
      }
      else if ((fb >= 0 && fc <= 0) || (fb <= 0 && fc >= 0)) {
        a  = c;
        fa = fc;
      }
      else
        return 1;
    }
    if (iter >= LGSEP_BISECTION_MAX_ITER)
      return 1;
    oRoot = a/2 + b/2;
    return 0;
}
//------------------------------------------------------------------------------------------------
unsigned FBSolveFourthDegreePolynomial(double* iCoeffs, double* oRoots, double iTolerance)
{
  double dCoeff[4] = {
    4*iCoeffs[0], 3*iCoeffs[1], 2*iCoeffs[2], iCoeffs[3]
  };
  if ( fabs(iCoeffs[0]) <= iTolerance)
    return FBSolveThirdDegreePolynomial(iCoeffs + 1, oRoots, iTolerance);
  double  dRoots[3];
  unsigned  dRootsNum = FBSolveThirdDegreePolynomial(dCoeff, dRoots, iTolerance);
  unsigned  pointsNum = dRootsNum + 2;
  std::vector<double> points(pointsNum);
  unsigned i=0/*, idx = 1*/;
  double   max_coeff = fabs(iCoeffs[1]);
  for (i=2;i<4;i++) {
    if (fabs(iCoeffs[i]) > max_coeff) {
      max_coeff = fabs(iCoeffs[i]);
    }
  }
  double  C = max_coeff / fabs(iCoeffs[0]) + 1;
  double  pointLeft = -C, pointRight = C;
  points[0] = pointLeft;
  for (i=0; i<dRootsNum;i++)
    points[i+1] = dRoots[i];
  points[dRootsNum + 1] = pointRight;
  std::vector<int> signs(pointsNum);
  for (i=0; i<pointsNum;i++)
    signs[i] = (LGSEP_EVAL_FOURTH_DEGREE_POLYNOMIAL(iCoeffs, points[i]) > 0) ? 1 : -1;
  unsigned root_num = 0;
  for (i=0;i<pointsNum-1;i++) {
    if (signs[i]*signs[i+1] < 0) {
      double root = 0.0;
      if ( !RootFindFourthDegreePolynomial(iCoeffs, points[i], points[i+1], root) ) {
        oRoots[root_num] = root;
        root_num++;
      }
    }
  }
  return root_num;
}

double LegendreCoeficientsTable[11][11] = 
{{1.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000},
 {0.00000000, 1.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000},
 {-0.5000000, 0.00000000, 1.50000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000},
 {0.00000000, -1.5000000, 0.00000000, 2.50000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000},
 {0.37500000, 0.00000000, -3.7500000, 0.00000000, 4.37500000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000},
 {0.00000000, 1.87500000, 0.00000000, -8.7500000, 0.00000000, 7.87500000, 0.00000000, 0.00000000, 0.00000000, 0.00000000, 0.00000000},
 {-0.3125000, 0.00000000, 6.56250000, 0.00000000, -19.687500, 0.00000000, 14.4375000, 0.00000000, 0.00000000, 0.00000000, 0.00000000},
 {0.00000000, -2.1875000, 0.00000000, 19.6875000, 0.00000000, -43.312500, 0.00000000, 26.8125000, 0.00000000, 0.00000000, 0.00000000},
 {0.27343750, 0.00000000, -9.8437500, 0.00000000, 54.1406250, 0.00000000, -93.843750, 0.00000000, 50.2734375, 0.00000000, 0.00000000},
 {0.00000000, 2.46093750, 0.00000000, -36.093750, 0.00000000, 140.765625, 0.00000000, -201.09375, 0.00000000, 94.9609375, 0.00000000},
 {-0.24609375, 0.0000000, 13.53515625, 0.0000000, -117.3046875, 0.000000, 351.9140625, 0.0000000, -427.32421875, 0.00000, 180.42578125}};


double* GetLegendrePolinomCoeficients(unsigned int iPolinomIDx)
{
  if(iPolinomIDx >= 11) return NULL;
  return LegendreCoeficientsTable[iPolinomIDx];
}

double EvaluateLegendrePolinomValue(double x, unsigned int iPolinomIDx)
{
  double* pCoef = GetLegendrePolinomCoeficients(iPolinomIDx);
  double x_pow = 1;
  double res = 0;

  for(unsigned int i = 0; i < iPolinomIDx + 1; i++) {
    res += pCoef[i] * x_pow;
    x_pow *= x;
  }

  return res;
}

unsigned int FBEvaluatePolynomialMult(const double* a, unsigned int aDeg, const double* b, unsigned int bDeg, double* opResult)
{
  unsigned int maxDeg = aDeg + bDeg;

  double sum = 0;
  for(int deg = 0; deg <= (int)maxDeg; deg++) {
    opResult[deg] = 0;

    for(int it = 0; it <= deg; it++) {
      if(deg - it > (int)bDeg) continue;
      if(deg - it < 0 || it > (int)aDeg) break;

      opResult[deg] += a[it] * b[deg - it];
    }
    sum += opResult[deg];
  }

  double coefTolerance = (sum / maxDeg) / 100; // 1% of average coef

  for(maxDeg = maxDeg; maxDeg >= 0; maxDeg--) {
    if(FBAbs(opResult[maxDeg]) > coefTolerance)
      break;
  }

  return maxDeg;
}

unsigned int FBElevatedBinomialCoefs[31][31] = {{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 2, 1, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0}, {1, 3, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 4, 6, 4, 1, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0}, {1, 5, 10, 10, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 6, 15, 20, 15, 6, 1, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 
  7, 21, 35, 35, 21, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 8, 28, 56, 70, 56, 28, 8, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 9, 
  36, 84, 126, 126, 84, 36, 9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 10, 45, 120, 210, 252, 210, 120, 45,
   10, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0}, {1, 11, 55, 165, 330, 462, 462, 330, 165, 55, 11, 1, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 12, 66, 220, 495,
   792, 924, 792, 495, 220, 66, 12, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0}, {1, 13, 78, 286, 715, 1287, 1716, 1716, 
  1287, 715, 286, 78, 13, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0}, {1, 14, 91, 364, 1001, 2002, 3003, 3432, 3003, 2002, 
  1001, 364, 91, 14, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0}, {1, 15, 105, 455, 1365, 3003, 5005, 6435, 6435, 5005, 3003, 
  1365, 455, 105, 15, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0}, {1, 16, 120, 560, 1820, 4368, 8008, 11440, 12870, 11440, 8008, 
  4368, 1820, 560, 120, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0}, {1, 17, 136, 680, 2380, 6188, 12376, 19448, 24310, 24310, 19448,
   12376, 6188, 2380, 680, 136, 17, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0}, {1, 18, 153, 816, 3060, 8568, 18564, 31824, 43758, 48620, 
  43758, 31824, 18564, 8568, 3060, 816, 153, 18, 1, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0}, {1, 19, 171, 969, 3876, 11628, 27132, 50388, 
  75582, 92378, 92378, 75582, 50388, 27132, 11628, 3876, 969, 171, 19,
   1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 20, 190, 1140, 4845, 
  15504, 38760, 77520, 125970, 167960, 184756, 167960, 125970, 77520, 
  38760, 15504, 4845, 1140, 190, 20, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0}, {1, 21, 210, 1330, 5985, 20349, 54264, 116280, 203490, 293930, 
  352716, 352716, 293930, 203490, 116280, 54264, 20349, 5985, 1330, 
  210, 21, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 22, 231, 1540, 7315, 
  26334, 74613, 170544, 319770, 497420, 646646, 705432, 646646, 
  497420, 319770, 170544, 74613, 26334, 7315, 1540, 231, 22, 1, 0, 0, 
  0, 0, 0, 0, 0, 0}, {1, 23, 253, 1771, 8855, 33649, 100947, 245157, 
  490314, 817190, 1144066, 1352078, 1352078, 1144066, 817190, 490314, 
  245157, 100947, 33649, 8855, 1771, 253, 23, 1, 0, 0, 0, 0, 0, 0, 
  0}, {1, 24, 276, 2024, 10626, 42504, 134596, 346104, 735471, 
  1307504, 1961256, 2496144, 2704156, 2496144, 1961256, 1307504, 
  735471, 346104, 134596, 42504, 10626, 2024, 276, 24, 1, 0, 0, 0, 0, 
  0, 0}, {1, 25, 300, 2300, 12650, 53130, 177100, 480700, 1081575, 
  2042975, 3268760, 4457400, 5200300, 5200300, 4457400, 3268760, 
  2042975, 1081575, 480700, 177100, 53130, 12650, 2300, 300, 25, 1, 0,
   0, 0, 0, 0}, {1, 26, 325, 2600, 14950, 65780, 230230, 657800, 
  1562275, 3124550, 5311735, 7726160, 9657700, 10400600, 9657700, 
  7726160, 5311735, 3124550, 1562275, 657800, 230230, 65780, 14950, 
  2600, 325, 26, 1, 0, 0, 0, 0}, {1, 27, 351, 2925, 17550, 80730, 
  296010, 888030, 2220075, 4686825, 8436285, 13037895, 17383860, 
  20058300, 20058300, 17383860, 13037895, 8436285, 4686825, 2220075, 
  888030, 296010, 80730, 17550, 2925, 351, 27, 1, 0, 0, 0}, {1, 28, 
  378, 3276, 20475, 98280, 376740, 1184040, 3108105, 6906900, 
  13123110, 21474180, 30421755, 37442160, 40116600, 37442160, 
  30421755, 21474180, 13123110, 6906900, 3108105, 1184040, 376740, 
  98280, 20475, 3276, 378, 28, 1, 0, 0}, {1, 29, 406, 3654, 23751, 
  118755, 475020, 1560780, 4292145, 10015005, 20030010, 34597290, 
  51895935, 67863915, 77558760, 77558760, 67863915, 51895935, 
  34597290, 20030010, 10015005, 4292145, 1560780, 475020, 118755, 
  23751, 3654, 406, 29, 1, 0}, {1, 30, 435, 4060, 27405, 142506, 
  593775, 2035800, 5852925, 14307150, 30045015, 54627300, 86493225, 
  119759850, 145422675, 155117520, 145422675, 119759850, 86493225, 
  54627300, 30045015, 14307150, 5852925, 2035800, 593775, 142506, 
  27405, 4060, 435, 30, 1}};

double FBBinomial(unsigned int iForm, unsigned int iBy)
{
  if(iForm < 31 && iBy < 31)
    return FBElevatedBinomialCoefs[iForm][iBy];
  if(iForm == 1)return iForm;
  return (FBFactorial(iForm))/(FBFactorial(iBy)*FBFactorial((iForm - iBy)));
}
