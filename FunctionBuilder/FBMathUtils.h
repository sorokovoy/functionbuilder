// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Math utilities
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBMathUtils_H
#define FBMathUtils_H

#include <math.h>

#define TOLERANCE       1.00E-006
#define ANGLE_TOLERANCE 1.00E-003

#define FBMax(a,b)    (((a) > (b)) ? (a) : (b))
#define FBMin(a,b)    (((a) < (b)) ? (a) : (b))
#define FBAbs(x)      ((x) < 0 ? -(x) : (x))
#define FBSqr(X)      ((X)*(X))

// constants
#define FBPi    3.14159265358979323846264338327950288419716939937510
#define FB2Pi   6.28318530717958647692528676655900576839433879875021
#define FBPiBy2 1.57079632679489661923132169163975144209858469968755
#define FBPiBy3 1.04719755119659774615421446109316762806572313312504
#define FBPiBy4 0.78539816339744830961566084581987572104929234984377

// FBRadian is 180.0/FBPi
#define FBRadian 57.29577951308232087679815481410517033240547246656432154916

#define FBBigNumber 1e100
#define FBBigFloatNumber 1.00E35f

#define FBIsInRange(x, iMin, iMax)  \
   ((iMin) <= (x) && (x) <= (iMax)) \

#define FBIsInRangeEx(x, iMin, iMax, iTolerance)  \
   ((iMin - iTolerance) <= (x) && (x) <= (iMax + iTolerance)) \

#define FBIsNAN(x)    ( _isnan(x) )
#define FBIsFinite(x) ( _finite((x)) != 0 )

#define FBTimerInit(TimerName)  time_t TimerName = time(NULL);
#define FBTimerReset(TimerName)  TimerName = time(NULL);
#define FBTimerGetDiff_sec(TimerName)  difftime(time(NULL), TimerName)

double FBPow(double x, unsigned int n);
double FBFactorial(unsigned int n);
double FBBinomial(unsigned int iForm, unsigned int iBy);
int FBHash(const char* ipString);

template<class ObjectType, typename WeightType>
struct FBNumberWeightedObject
{
  FBNumberWeightedObject(ObjectType iValue, WeightType iWeight)
    :_object(iValue)
  {
    _weight = iWeight;
  }

  ObjectType _object;
  WeightType _weight;
  bool operator < (const FBNumberWeightedObject<ObjectType, WeightType>& iValue) const { return _weight < iValue._weight; }
};

template<typename NumberType>
struct FBZerowedNum
{
  FBZerowedNum()
  {
    val = 0;
  }
  NumberType val;
};

template <class T>
inline void FBSwap(T& iValue1, T& iValue2)
{
  T tmp = iValue1;
  iValue1 = iValue2;
  iValue2 = tmp;
}

inline bool FBEqual(double iX, double iY, double iTol = TOLERANCE)
{
  return FBAbs(iX - iY) < iTol;
}

inline bool FBLess(double iX, double iY, double iTol = TOLERANCE)
{
  return iX - iY < -iTol;
}

inline bool FBLessEqual(double iX, double iY, double iTol = TOLERANCE)
{
  return iX - iY < iTol;
}

inline int FBRound(double iVal)
{
  return (int)(iVal + 0.5);
}

inline double FBSign(double iX, double iTol = TOLERANCE)
{
  if(FBEqual(iX, 0, iTol))
    return 0;
  return iX > 0 ? 1 : -1;
}

class FBLessTol
{
  public:
    FBLessTol(double iTol) { _tolerance = iTol; }
    bool operator() (double iX, double iY) const { return iX - iY < -_tolerance; }
  private:
    double _tolerance;
};

class FBLessEqualTol
{
  public:
    FBLessEqualTol(double iTol) { _tolerance = iTol; }
    bool operator()(double iX, double iY) const { return iX - iY < _tolerance; }
  private:
    double _tolerance;
};

inline double FBSqrt(double iX)
{
  return (iX <= 0.0) ? 0.0 : sqrt(iX);
}

// adjust angle into range [0, 2PI]
inline double FBGetAdjustedAngle(double iAngle)
{
  return iAngle - floor(iAngle / FB2Pi) * FB2Pi;
}

//iCoeffs[0]*x + iCoeffs[1] = 0
unsigned int FBSolveFirstDegreePolynomial(double* iCoeffs, double* oRoots, double iTolerance);

//iCoeffs[0]*x*x + iCoeffs[1]*x + iCoeffs[2] = 0
unsigned int FBSolveSecondDegreePolynomial(double* iCoeffs, double* oRoots, double iTolerance);

//iCoeffs[0]*x*x*x + iCoeffs[1]*x*x + iCoeffs[2]*x + iCoeffs[3] = 0
unsigned int FBSolveThirdDegreePolynomial(double* iCoeffs, double* oRoots, double iTolerance);

//iCoeffs[0]*x*x*x*x + iCoeffs[1]*x*x*x + iCoeffs[2]*x*x + iCoeffs[3]*x + iCoeffs[4] = 0
unsigned int FBSolveFourthDegreePolynomial(double* iCoeffs, double* oRoots, double iTolerance);

//http://en.wikipedia.org/wiki/Legendre_polynomials
double* GetLegendrePolinomCoeficients(unsigned int iPolinomIDx);
double EvaluateLegendrePolinomValue(double iArg, unsigned int iPolinomIDx);

//(a[0] + a[1]*x + a[2]*x^2 + a[3]*x^3 + ... + a[aDeg]*x^aDeg) * (a[0] + a[1]*x + a[2]*x^2 + a[3]*x^3 + ... + a[aDeg]*x^aDeg) = opResult[0] + opResult[1]*x + opResult[2]*x^2 + ... + opResult[aDeg]*x^(retValue)
unsigned int FBEvaluatePolynomialMult(const double* a, unsigned int aDeg, const double* b, unsigned int bDeg, double* opResult);

#endif // FBMathUtils_H
