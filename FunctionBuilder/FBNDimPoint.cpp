// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Implement n dim point.
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBNDimPoint.h"
#include "FBAssert.h"

FBNDimPoint::FBNDimPoint(const FBNDimPoint& iVal)
:_dim(iVal._dim), _data(iVal._data)
{}

FBNDimPoint::FBNDimPoint(FBAutoPtr<const double> iData, unsigned int iDim)
:_dim(iDim)
{
  _data = iData;
}

FBNDimPoint::~FBNDimPoint()
{}

FBNDimPoint& FBNDimPoint::operator = (const FBNDimPoint& iVal)
{
  if(this == &iVal) {
    return *this;
  }
  _data = iVal._data;
  _dim = iVal._dim;
  return *this;
}

bool FBNDimPoint::GetDistanceTo(const FBNDimPoint* iPoint, double& oData) const
{
  FBAssertV(GetSquareDistanceTo(iPoint, oData), false);
  FBSqrt(oData);
  return true;
}

bool FBNDimPoint::GetSquareDistanceTo(const FBNDimPoint* iPoint, double& oData) const
{
  FBAssertV(_dim == iPoint->_dim, false);
  oData = 0;
  for(unsigned int i = 0; i < _dim; i++)
    oData += FBSqr(_data[i] - iPoint->_data[i]);
  return true;
}

bool FBNDimPoint::IsEqual(const FBNDimPoint& iWithPoint, double iTolerance) const
{
  if(iWithPoint._data.GetPointer() == _data.GetPointer())
    return true;

  double distance;
  FBAssertV(GetDistanceTo(&iWithPoint, distance), false);
  return FBEqual(distance, 0, iTolerance);
}

std::ostream& operator << (std::ostream& out, const FBNDimPoint* ipPoint)
{
  out << "Point " << ipPoint->GetDim() << " data ";
  for(unsigned int i = 0; i < ipPoint->GetDim(); i++) {
    out << ipPoint->GetData()[i]  << " ";
  }
  return out;
}

std::ostream& operator << (std::ostream& out, const FBNDimPoint& ipPoint)
{
  return out << &ipPoint;
}

bool FBNDimPointLessTol::operator() (const FBNDimPoint& iX, const FBNDimPoint& iY) const
{
  if(iX.GetDim() != iY.GetDim())
    return iX.GetDim() < iY.GetDim();

  for(unsigned int i = 0; i < iY.GetDim(); i++) {
    if(!FBEqual(iX.GetData()[i], iY.GetData()[i], _tolerance))
      return iX.GetData()[i] < iY.GetData()[i];
  }
  return false;
}

