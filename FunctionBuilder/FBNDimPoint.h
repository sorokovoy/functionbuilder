// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Implement n dim point.
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBNDimPoint_H
#define FBNDimPoint_H

#include "FBPrecompiled.h"
#include "FBMathUtils.h"
#include "FBAutoPtr.h"

class FBNDimPoint
{
public:
  FBNDimPoint(FBAutoPtr<const double> ipData, unsigned int iDim);
  FBNDimPoint(const FBNDimPoint& iVal);

  FBNDimPoint& operator = (const FBNDimPoint& iVal);

  ~FBNDimPoint();

  bool GetDistanceTo(const FBNDimPoint* iPoint, double& oData) const;
  bool GetSquareDistanceTo(const FBNDimPoint* iPoint, double& oData) const;

  bool IsEqual(const FBNDimPoint& iWithPoint, double iTolerance) const;

  unsigned int GetDim() const { return _dim; }
  FBAutoPtr<const double> GetData() const { return _data; }

private:
  unsigned int _dim;
  FBAutoPtr<const double> _data;
};

std::ostream& operator << (std::ostream& out, const FBNDimPoint* ipPoint);
std::ostream& operator << (std::ostream& out, const FBNDimPoint& ipPoint);

class FBNDimPointLessTol
{
  public:
    FBNDimPointLessTol(double iTol) { _tolerance = iTol; }
    bool operator() (const FBNDimPoint& iX, const FBNDimPoint& iY) const;
  private:
    double _tolerance;
};

#endif // FBNDimPoint_H
