// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Implement n dim rectangle
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBNDimRectangle.h"
#include "FBNDimPoint.h"
#include "FBAssert.h"
#include "FBMathUtils.h"

FBNDimRectangle::FBNDimRectangle(const FBNDimPoint& ipPoint1, const FBNDimPoint& ipPoint2, double iTolerance)
:_dim(ipPoint1.GetDim()),
 _tolerance(iTolerance)
{
  FBAssertR(ipPoint1.GetDim() == ipPoint2.GetDim());
  _maxes = new double[_dim];
  _mines = new double[_dim];
  for(unsigned int i = 0; i < _dim; i++) {
    _maxes[i] = FBMax(ipPoint1.GetData()[i], ipPoint2.GetData()[i]);
    _mines[i] = FBMin(ipPoint1.GetData()[i], ipPoint2.GetData()[i]);
  }
  _pLowPoint = new FBNDimPoint(_mines, _dim);
  _pHightPoint = new FBNDimPoint(_maxes, _dim);

  _volume = NULL;
}

FBNDimRectangle::~FBNDimRectangle()
{
  delete _volume;
  delete _pLowPoint;
  delete _pHightPoint;
}

double FBNDimRectangle::CanculateVolume(const FBNDimPoint& ipPoint1, const FBNDimPoint& ipPoint2, double iTolerance)
{
  FBAssertV(ipPoint1.GetDim() == ipPoint2.GetDim(), -1);
  double _volume = 1;
  for(unsigned int i = 0; i < ipPoint1.GetDim(); i++) {
    _volume *= FBMax(FBAbs(ipPoint1.GetData()[i] - ipPoint2.GetData()[i]), iTolerance);
  }
  return _volume;
}

double FBNDimRectangle::GetVolume() const
{
  if(_volume == NULL) {
    _volume = new double;
    *_volume = CanculateVolume(*_pLowPoint, *_pHightPoint);
  }
  return *_volume;
}

bool FBNDimRectangle::IsContainsInSubspace(const FBNDimPoint& ipPoint, unsigned int iSubspaceIdx) const
{
  FBAssertV(iSubspaceIdx + ipPoint.GetDim() <= _dim, false);
  for(unsigned int i = 0; i < ipPoint.GetDim(); i++) {
    unsigned int idx = iSubspaceIdx + i;
    if(FBLess(ipPoint.GetData()[idx], _mines[idx], _tolerance) || FBLess(_maxes[idx], ipPoint.GetData()[idx], _tolerance))
      return false;
  }
  return true;
}

bool FBNDimRectangle::IsContainsInSubspace(const double* ipPointCoords, unsigned int iSubSpaceDim, unsigned int iSubspaceIdx) const
{
  FBAssertV(iSubspaceIdx + iSubSpaceDim <= _dim, false);
  for(unsigned int i = 0; i < iSubSpaceDim; i++) {
    unsigned int idx = iSubspaceIdx + i;
    if(FBLess(ipPointCoords[idx], _mines[idx], _tolerance) || FBLess(_maxes[idx], ipPointCoords[idx], _tolerance))
      return false;
  }
  return true;
}

bool FBNDimRectangle::IsContains(const FBNDimPoint& ipPoint) const
{
  FBAssertV(_dim == ipPoint.GetDim(), false);
  return IsContains(ipPoint.GetData());
}

bool FBNDimRectangle::IsContains(const double* ipPointCoords) const
{
  for(unsigned int i = 0; i < _dim; i++)
    if(FBLess(ipPointCoords[i], _mines[i], _tolerance) || FBLess(_maxes[i], ipPointCoords[i], _tolerance))
      return false;
  return true;
}

bool FBNDimRectangle::IsContains(const FBNDimRectangle& iRect) const
{
  FBAssertV(_dim == iRect._dim, false);
  for(unsigned int i = 0; i < _dim; i++)
    if(FBLess(iRect._mines[i], _mines[i], _tolerance) || FBLess(_maxes[i], iRect._maxes[i], _tolerance))
      return false;
  return true;
}

std::vector<FBNDimRectangle*> FBNDimRectangle::CreateSeparation(const FBNDimPoint& ipPoint) const
{
  std::vector<FBNDimRectangle*> resalt;
  if(!IsContains(ipPoint)) return resalt;

  resalt.push_back(new FBNDimRectangle(*_pLowPoint, ipPoint, _tolerance));
  resalt.push_back(new FBNDimRectangle(*_pHightPoint, ipPoint, _tolerance));
  return resalt;
}

bool FBNDimRectangle::operator == (const FBNDimRectangle& iWithRect) const
{
  if(_dim != iWithRect._dim) return false;
  for(unsigned int i = 0; i < _dim; i++) {
    if(!FBEqual(_maxes[i], iWithRect._maxes[i], _tolerance)) return false;
    if(!FBEqual(_mines[i], iWithRect._mines[i], _tolerance)) return false;
  }
  return true;
}

bool FBNDimRectangle::operator <  (const FBNDimRectangle& iWithRect) const
{
  if(_dim != iWithRect._dim) return  _dim < iWithRect._dim;
  for(unsigned int i = 0; i < _dim; i++) {
    if(!FBEqual(_maxes[i], iWithRect._maxes[i], _tolerance)) return _maxes[i] < iWithRect._maxes[i];
    if(!FBEqual(_mines[i], iWithRect._mines[i], _tolerance)) return _mines[i] < iWithRect._mines[i];
  }
  return true;
}

