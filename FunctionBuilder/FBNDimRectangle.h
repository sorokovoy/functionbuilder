// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Implement n dim rectangle
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBNDimRectangle_H
#define FBNDimRectangle_H

#include "FBMathUtils.h"
#include "FBPrecompiled.h"

class FBNDimPoint;

class FBNDimRectangle
{
public:
  FBNDimRectangle(const FBNDimPoint& ipPoint1, const FBNDimPoint& ipPoint2, double iTolerance = TOLERANCE);
  ~FBNDimRectangle();

  bool IsContainsInSubspace(const FBNDimPoint& ipPoint, unsigned int iSubspaceIdx) const;
  bool IsContainsInSubspace(const double* ipPointCoords, unsigned int iSubSpaceDim, unsigned int iSubspaceIdx) const;

  bool IsContains(const FBNDimPoint& ipPoint) const;
  bool IsContains(const double* ipPointCoords) const;

  bool IsContains(const FBNDimRectangle& iRect) const;

  std::vector<FBNDimRectangle*> CreateSeparation(const FBNDimPoint& ipPoint) const;

  bool operator == (const FBNDimRectangle& iWithRect) const;
  bool operator <  (const FBNDimRectangle& iWithRect) const;

  double GetVolume() const;

  static double CanculateVolume(const FBNDimPoint& ipPoint1, const FBNDimPoint& ipPoint2, double iTolerance = TOLERANCE);

  FBNDimPoint* GetLowPoint() const { return _pLowPoint; }
  FBNDimPoint* GetHightPoint() const { return _pHightPoint; }

private:
  mutable double* _volume;

  FBNDimRectangle(): _tolerance(TOLERANCE), _dim(0) {};
  FBNDimRectangle(const FBNDimRectangle&): _tolerance(TOLERANCE), _dim(0) {};

  const double _tolerance;
  const unsigned int _dim;
  double* _maxes;
  double* _mines;

  FBNDimPoint* _pLowPoint;
  FBNDimPoint* _pHightPoint;
};

#endif // FBNDimRectangle_H
