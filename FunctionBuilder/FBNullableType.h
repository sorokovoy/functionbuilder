// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// implement nullable type 
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBNullableType_H
#define FBNullableType_H

template <class T>
class FBNullableType
{
public:
  FBNullableType(const T& iValue)
  {
    _storedValue = iValue;
    _hasValue = true;
  }

  FBNullableType()
  {
    _storedValue = 0;
    _hasValue = false;
  }

  FBNullableType& operator = (const FBNullableType& iValue)
  {
    _hasValue = iValue._hasValue;
    _storedValue = iValue._storedValue;
    return *this;
  }

  bool HasValue() const
  {
    return _hasValue;
  }

  T& GetValue()
  {
    return _storedValue;
  }

private:
  bool _hasValue;
  T _storedValue;
};

#endif // FBFixedSizeBuffer_H
