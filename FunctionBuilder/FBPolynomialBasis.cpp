// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Basis for build simple polinomes 
// x + x^2 + ... + x^(iBasisLenght-1)
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBPolynomialBasis.h"
#include "FBMathUtils.h"

FBPolynomialBasis::FBPolynomialBasis(const std::vector<double>& iDegs)
:_degs(iDegs)
{
  _isAllDegsInteger = true;
  _maxDeg = - FBBigNumber;
  for(unsigned int i = 0; i < _degs.size(); i++) {
    if(_degs[i] < -TOLERANCE || _degs[i] - floor(_degs[i] + TOLERANCE) > TOLERANCE) {
      _isAllDegsInteger = false;
    }
    _maxDeg = FBMax(_maxDeg, _degs[i]);
  }
}

FBPolynomialBasis::FBPolynomialBasis(int iSimplePolinomMaxDeg)
{
  for(int i = 1; i <= iSimplePolinomMaxDeg; i++)
    _degs.push_back((double)i);
  _isAllDegsInteger = true;
  _maxDeg = iSimplePolinomMaxDeg;
}

unsigned int FBPolynomialBasis::GetVarCountPerElement() const
{
  return _degs.size();
}

double FBPolynomialBasis::CalculateValueByIdx(double iValue, int idx) const
{
  return pow(iValue, _degs[idx]);
}

IFBFunctionBasis* FBPolynomialBasis::CreateDerivaty() const
{
  std::vector<double> newDegs;
  for(unsigned int i = 0; i < _degs.size(); i++)
    newDegs.push_back(_degs[i] - 1);
  return new FBPolynomialBasis(newDegs);
}

IFBFunctionBasis* FBPolynomialBasis::CreateCopy() const
{
  return new FBPolynomialBasis(_degs);
}

double FBPolynomialBasis::TransformDerivatyCoeficients(double* ioCoefs) const
{
  for(unsigned int i = 0; i < _degs.size(); i++)
    ioCoefs[i] *= _degs[i];
  return 0;
}

std::string FBPolynomialBasis::ShowDependencyType(int idx, const char* varName) const
{
  char buf[255];
  sprintf_s(buf, 255, " %s^%.1f ", varName, _degs[idx]);
  return buf;
}
