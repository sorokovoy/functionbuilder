// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Basis for build simple polynomes 
// x + x^2 + ... + x^(iBasisLenght-1)
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBPolynomialBasis_h
#define FBPolynomialBasis_h

#include "IFBFunctionBasis.h"

class FBPolynomialBasis : public IFBFunctionBasis
{
public:
  FBPolynomialBasis(int iSimplePolinomMaxDeg);
  FBPolynomialBasis(const std::vector<double>& iDegs);
  bool IsAllDegsInteger() const { return _isAllDegsInteger; }
  double GetMaxDeg() const { return _maxDeg; }
  const std::vector<double>& GetDegs() const { return _degs; }

  virtual unsigned int GetVarCountPerElement() const;
  virtual double CalculateValueByIdx(double iValue, int idx) const;
  virtual IFBFunctionBasis* CreateDerivaty() const;
  virtual IFBFunctionBasis* CreateCopy() const;
  virtual double TransformDerivatyCoeficients(double* ioCoefs) const;
  virtual std::string ShowDependencyType(int idx, const char* varName) const;
private:
  bool _isAllDegsInteger;
  double _maxDeg;
  std::vector<double> _degs;
};

#endif // FBPolynomialBasis_h
