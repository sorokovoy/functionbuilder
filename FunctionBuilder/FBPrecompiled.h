// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Include all standard library s for increase compilation speed
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include <algorithm>
#include <vector>
#include <stack>
#include <list>
#include <deque>
#include <memory>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <set>
#include <map>

#include <cmath>
#include <ctime>
#include <io.h>

#include <math.h>
#include <float.h>

#include <typeinfo> // C++ reflection tools

