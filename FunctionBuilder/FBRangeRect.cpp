// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// implement object containner for generalize marked points 
// in rectangle storage 
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBRangeRect.h"
#include "FBAssert.h"

struct FBRangeRectPointInfo
{
  FBRangeRectPointInfo(FBNDimPoint* cipPoint, bool iMark) { _pPoint = cipPoint; _mark = iMark; }
  ~FBRangeRectPointInfo() { delete _pPoint; }

  FBNDimPoint* _pPoint;
  bool _mark;
};

struct FBRangeRectRectInfo
{
  ~FBRangeRectRectInfo() { delete _pRect; }
  FBNDimRectangle* _pRect;
  FBRangeRectPointInfo* _pPoint1;
  FBRangeRectPointInfo* _pPoint2;
};

FBRangeRect::FBRangeRect(unsigned int iDim, bool iDefaultMark, double iTolerance)
:_dim(iDim),
 _tolerance(iTolerance),
 _defaultMark(iDefaultMark)
{
  _minPoint = NULL;
  _maxPoint = NULL;
  _pRanges = new FBArrayAgregator<double>[iDim];
  _pMaxes = NULL;
  _pMines = NULL;
}

FBRangeRect::FBRangeRect(double* cipMinData, double* cipMaxData, unsigned int iDim, bool iDefaultMark, double iTolerance)
:_dim(iDim),
 _tolerance(iTolerance),
 _defaultMark(iDefaultMark)
{
  InitializeByData(cipMinData, cipMaxData);
}

void FBRangeRect::InitializeByData(double* cipMinData, double* cipMaxData)
{
  _pRanges = NULL;
  _pMaxes = NULL;
  _pMines = NULL;

  _minPoint = new FBNDimPoint(cipMinData, _dim);
  _maxPoint = new FBNDimPoint(cipMaxData, _dim);
  _storedPoints.push_back(new FBRangeRectPointInfo(_minPoint, _defaultMark));
  _storedPoints.push_back(new FBRangeRectPointInfo(_maxPoint, _defaultMark));
}

FBRangeRect::~FBRangeRect()
{
  Reset();
  delete _pRanges;
}

void FBRangeRect::Reset()
{
  for(std::list<FBRangeRectRectInfo*>::iterator it = _storedRectangles.begin(); it != _storedRectangles.end(); it++)
    delete *it;

  std::vector<FBRangeRectPointInfo*> eraseExceptions;

  for(std::list<FBRangeRectPointInfo*>::iterator it = _storedPoints.begin(); it != _storedPoints.end(); it++) {
    if((*it)->_pPoint == _maxPoint || (*it)->_pPoint == _minPoint) {
      eraseExceptions.push_back(*it);
      continue;
    }
    delete *it;
  }
  _storedRectangles.clear();
  _storedPoints.clear();

  for(unsigned int i = 0; i < eraseExceptions.size(); i++) {
    _storedPoints.push_back(eraseExceptions[i]);
  }

  if(_pRanges != NULL) {
    for(unsigned int i = 0; i < _dim; i++) {
      _pRanges[i].reset();
    }
  }
}

void FBRangeRect::AppendPoint(FBNDimPoint* cipPoint, bool iMark)
{
  FBAssertR(_dim == cipPoint->GetDim());

  if(_pRanges != NULL) {
    for(unsigned int i = 0; i < _dim; i++) {
      _pRanges[i].ProcessValue(cipPoint->GetData()[i]);
    }
  }

  FBRangeRectPointInfo* newPointInfo = NULL;
  for(std::list<FBRangeRectPointInfo*>::iterator it = _storedPoints.begin(); it != _storedPoints.end(); it++) {
    if((*it)->_pPoint->IsEqual(*cipPoint, _tolerance)) {
      if((*it)->_mark == iMark) {
        delete cipPoint;
        return;
      }
      (*it)->_mark = iMark;
      delete cipPoint;
      cipPoint = (*it)->_pPoint;
      newPointInfo = *it;
      break;
    }
  }

  if(newPointInfo == NULL) {
    newPointInfo = new FBRangeRectPointInfo(cipPoint, iMark);
    _storedPoints.push_back(newPointInfo);
  }

  if(iMark == _defaultMark) {
    ValidateExistRectangles(newPointInfo);
  } else {
    bool addRectangleRequired = true;
    for(std::list<FBRangeRectRectInfo*>::iterator it = _storedRectangles.begin(); it != _storedRectangles.end(); it++) {
      if((*it)->_pRect->IsContains(*cipPoint)) {
        addRectangleRequired = false;
        break;
      }
    }

    if(addRectangleRequired)
      TryGenerateNewRectangle(newPointInfo, iMark);
  }
}

void FBRangeRect::ValidateExistRectangles(FBRangeRectPointInfo* iNewPointInfo)
{
  std::vector<std::list<FBRangeRectRectInfo*>::iterator> forErase;
  for(std::list<FBRangeRectRectInfo*>::iterator it = _storedRectangles.begin(); it != _storedRectangles.end(); it++) {
    if((*it)->_pRect->IsContains(*iNewPointInfo->_pPoint)) {
      forErase.push_back(it);
    }
  }

  for(unsigned int i = 0; i < forErase.size(); i++) {
    delete *forErase[i];
    _storedRectangles.erase(forErase[i]);
  }
}

bool FBRangeRect::GetMark(const FBNDimPoint& ipPoint)
{
  FBAssertV(_dim == ipPoint.GetDim(), _defaultMark);
  return GetMark(ipPoint.GetData());
}

bool FBRangeRect::GetMark(const double* ipDataPoint)
{
  for(std::list<FBRangeRectRectInfo*>::iterator it = _storedRectangles.begin(); it != _storedRectangles.end(); it++) {
    if((*it)->_pRect->IsContains(ipDataPoint))
      return !_defaultMark;
  }
  return _defaultMark;
}

const double* FBRangeRect::GetMaxes() const
{
  if(_maxPoint != NULL) {
    return _maxPoint->GetData();
  }

  if(_pMaxes == NULL)
    _pMaxes = new double[_dim];

  for(unsigned int i = 0; i < _dim; i++)
    _pMaxes[i] = _pRanges[i].GetMax();

  return _pMaxes;
}

const double* FBRangeRect::GetMines() const
{
  return _minPoint->GetData();

  if(_pMines == NULL)
    _pMines = new double[_dim];

  for(unsigned int i = 0; i < _dim; i++)
    _pMines[i] = _pRanges[i].GetMin();

  return _pMines;
}

void FBRangeRect::TryGenerateNewRectangle(FBRangeRectPointInfo* iNewPointInfo, bool iMark)
{
  double bigestVolume = 0;
  FBRangeRectPointInfo* bestPoint = NULL;

  for(std::list<FBRangeRectPointInfo*>::iterator it = _storedPoints.begin(); it != _storedPoints.end(); it++) {
    if((*it)->_mark != iMark || *it == iNewPointInfo) continue;

    double newValue = FBNDimRectangle::CanculateVolume(*iNewPointInfo->_pPoint, *(*it)->_pPoint, _tolerance);
    if( bigestVolume < newValue) {
      FBNDimRectangle* newRect = new FBNDimRectangle(*iNewPointInfo->_pPoint, *(*it)->_pPoint, _tolerance);
      bool conflictFound = false;
      for(std::list<FBRangeRectPointInfo*>::iterator it1 = _storedPoints.begin(); it1 != _storedPoints.end(); it1++) {
        if(iMark != (*it1)->_mark && newRect->IsContains(*(*it1)->_pPoint)) {
          conflictFound = true;
          break;
        }
      }
      delete newRect;

      if(!conflictFound) {
        bigestVolume = newValue;
        bestPoint = *it;
      }
    }
  }

  if(bestPoint == NULL) return;

  FBNDimRectangle* newRect = new FBNDimRectangle(*iNewPointInfo->_pPoint, *bestPoint->_pPoint, _tolerance);

  std::vector<std::list<FBRangeRectRectInfo*>::iterator> forErase;
  for(std::list<FBRangeRectRectInfo*>::iterator it = _storedRectangles.begin(); it != _storedRectangles.end(); it++) {
    if(newRect->IsContains(*(*it)->_pRect)) {
      delete *it;
      forErase.push_back(it);
    }
  }

  for(unsigned int i = 0; i < forErase.size(); i++)
    _storedRectangles.erase(forErase[i]);

  FBRangeRectRectInfo* newRectInfo = new FBRangeRectRectInfo();
  newRectInfo->_pRect = newRect;
  newRectInfo->_pPoint1 = bestPoint;
  newRectInfo->_pPoint2 = iNewPointInfo;
  _storedRectangles.push_back(newRectInfo);
}

