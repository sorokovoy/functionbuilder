// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// implement object containner for generalize marked points 
// in rectangle storage 
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBRangeRect_H
#define FBRangeRect_H

#include "FBPrecompiled.h"

#include "FBArrayAgregator.h"

#include "FBNDimPoint.h"
#include "FBNDimRectangle.h"

struct FBRangeRectPointInfo;
struct FBRangeRectRectInfo;

class FBRangeRect
{
public:
  FBRangeRect(unsigned int iDim, bool iDefaultMark, double iTolerance);
  FBRangeRect(double* cipMinData, double* cipMaxData, unsigned int iDim, bool iDefaultMark, double iTolerance);
  ~FBRangeRect();

  void AppendPoint(FBNDimPoint* cipPoint, bool iMark);
  bool GetMark(const FBNDimPoint& ipPoint);
  bool GetMark(const double* ipPointCoords);

  double GetTolerance() const { return _tolerance; }

  void Reset();

  unsigned int GetDim() const { return _dim; }
  const double* GetMaxes() const;
  const double* GetMines() const;

private:
  void ValidateExistRectangles(FBRangeRectPointInfo* cipPoint);
  void TryGenerateNewRectangle(FBRangeRectPointInfo* iNewPointInfo, bool iMark);

  void InitializeByData(double* cipMinData, double* cipMaxData);

  const unsigned int _dim;
  const double _tolerance;
  const bool _defaultMark;

  FBNDimPoint* _minPoint;
  FBNDimPoint* _maxPoint;

  FBArrayAgregator<double>* _pRanges;
  mutable double* _pMaxes;
  mutable double* _pMines;

  std::list<FBRangeRectPointInfo*> _storedPoints;
  std::list<FBRangeRectRectInfo* > _storedRectangles;
};

#endif //FBRangeRect_H
