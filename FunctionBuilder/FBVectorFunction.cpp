// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Object vector function aproximator
//
//===================================================================
//  APR 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "IFBFunctionBasisSource.h"

#include "FBFunction.h"
#include "FBFunctionBuilder.h"
#include "FBVectorFunction.h"

FBVectorFunction::FBVectorFunction(IFBFunctionBasisSource* cipSourceBasisSource, unsigned int iArgumentSize, unsigned int iValueSize, double iTolerance)
:_argumentSize(iArgumentSize),
 _valueSize(iValueSize),
 _tolerance(iTolerance)
{
  _basisSources.push_back(cipSourceBasisSource);
  _scalarFunctions.push_back(new FBFunctionBuilder(cipSourceBasisSource->CreateBasis(), _argumentSize, _tolerance, cipSourceBasisSource->IsFunctionHasConstant()));
  for(unsigned int i = 1; i < _valueSize; i++) {
    _basisSources.push_back(cipSourceBasisSource->CreateCopy());
    _scalarFunctions.push_back(new FBFunctionBuilder(cipSourceBasisSource->CreateBasis(), _argumentSize, _tolerance, cipSourceBasisSource->IsFunctionHasConstant()));
  }
  _isRedy = false;
}

FBVectorFunction::~FBVectorFunction()
{
  for(unsigned int i = 0; i < _valueSize; i++) {
    delete _basisSources[i];
    delete _scalarFunctions[i];
  }
}

bool FBVectorFunction::AddApproximationPoint(const FBVectorFunctionValue& iValue)
{
  bool result = false;
  _isRedy = true;
  for(unsigned int i = 0; i < _valueSize; i++) {
    FBFunctionBuilder* pCurrScalarFunc = _scalarFunctions[i];

    unsigned int BlockIdx = 0;
    unsigned int ElementIdx = 0;

    double currScalarValue = iValue.elevatedValue.arg[BlockIdx].GetData()[ElementIdx];
    if(ElementIdx == iValue.elevatedValue.arg[BlockIdx].GetDim() && iValue.elevatedValue.arg.size() != 1) {
       ElementIdx = 0;
       BlockIdx++;
    }

    if(pCurrScalarFunc->AddApproximationPoint(FBFunctionPointData(iValue.argument.arg, currScalarValue))) {
      result = true;
      if(pCurrScalarFunc->EvaluateFunction() == NULL) {
        IFBFunctionBasisSource* pCurrBasisSource = _basisSources[i];
        if(pCurrBasisSource->IsDegIncrementRequired(pCurrScalarFunc->GetContainedPointCount(), pCurrScalarFunc->GetRequirePointCount(), pCurrScalarFunc->GetValidateSampleSetSize())) {
          pCurrBasisSource->IncrementDeg();
          FBFunctionBuilder* pNewFunctionBuilder = new FBFunctionBuilder(pCurrBasisSource->CreateBasis(), _argumentSize, _tolerance, pCurrBasisSource->IsFunctionHasConstant());

          const std::list<FBFunctionPointData>& currAccamulatedData = pCurrScalarFunc->GetData();
          for(std::list<FBFunctionPointData>::const_iterator it = currAccamulatedData.begin(); it != currAccamulatedData.end(); it++) {
            pNewFunctionBuilder->AddApproximationPoint(*it);
          }

          _scalarFunctions[i] = pNewFunctionBuilder;
          delete pCurrScalarFunc;

          if(pNewFunctionBuilder->EvaluateFunction() == NULL) {
            _isRedy = false;
          }
        } else {
          _isRedy = false;
        }
      }
    }
  }
  return result;
}

bool FBVectorFunction::EvaluateFunctionValue(FBVectorFunctionValue& iValue) const
{
  if(!_isRedy) return false;
  double* pElevatedValue = new double[_valueSize];

  for(unsigned int i = 0; i < _valueSize; i++) {
    FBFunctionBuilder* pCurrScalarFunc = _scalarFunctions[i];
    if(!pCurrScalarFunc->EvaluateFunction()->EvaluateFunctionValue(iValue.argument, pElevatedValue[i])) {
      delete pElevatedValue;
      return false;
    }
  }
  iValue.elevatedValue.arg.push_back(FBNDimPoint(pElevatedValue, _valueSize));
  return true;
}