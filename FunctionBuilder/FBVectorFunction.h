// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Object vector function aproximator
//
//===================================================================
//  APR 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBVectorFunction_h
#define FBVectorFunction_h

#include "FBVectorFunctionValue.h"

#include <vector>

class IFBFunctionBasisSource;

class FBFunctionBuilder;

class FBVectorFunction
{
public:
  FBVectorFunction(IFBFunctionBasisSource* cipSourceBasisSource, unsigned int iArgumentSize, unsigned int iValueSize, double iTolerance);
  ~FBVectorFunction();

  bool AddApproximationPoint(const FBVectorFunctionValue& iValue);
  bool EvaluateFunctionValue(FBVectorFunctionValue& iValue) const;

  bool IsRedy() const { return _isRedy; }

  unsigned int GetArgumentSize() const { return _argumentSize; }
  unsigned int GetValueSize() const { return _valueSize; }

protected:
  std::vector<IFBFunctionBasisSource*> _basisSources;
  std::vector<FBFunctionBuilder*> _scalarFunctions;

  bool _isRedy;

  const unsigned int _argumentSize;
  const unsigned int _valueSize;
  const double _tolerance;
};

#endif // IFBFunctionBasisSource_h
