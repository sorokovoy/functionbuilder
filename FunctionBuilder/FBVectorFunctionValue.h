// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Define vector function value. Used for put approximation values to
// vector function object and return elevated value from their
//
//===================================================================
//  APR 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBVectorFunctionValue_h
#define FBVectorFunctionValue_h

#include "FBAutoPtr.h"
#include "FBFunctionValue.h"

struct FBVectorFunctionValue
{
  FBFunctionArgument argument;
  FBFunctionArgument elevatedValue;
};

#endif // FBVectorFunctionValue_h
