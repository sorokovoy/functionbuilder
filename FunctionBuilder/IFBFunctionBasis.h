// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// definition of function basis
//
//===================================================================
//  APR 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef IFBFunctionBasis_H
#define IFBFunctionBasis_H

#include <string>
#include <vector>

class IFBFunctionBasis
{
public:
  virtual unsigned int GetVarCountPerElement() const = 0;
  virtual double CalculateValueByIdx(double iValue, int idx) const = 0;
  virtual IFBFunctionBasis* CreateDerivaty() const = 0;
  virtual IFBFunctionBasis* CreateCopy() const = 0;
  virtual double TransformDerivatyCoeficients(double* ioCoefs) const = 0;
  virtual std::string ShowDependencyType(int idx, const char* varName) const = 0;
};

#endif // IFBFunctionBasis_H
