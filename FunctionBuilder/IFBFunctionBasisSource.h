// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// base interface for basis generator object 
//
//===================================================================
//  APR 2014  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef IFBFunctionBasisSource_h
#define IFBFunctionBasisSource_h

#include <vector>
#include "FBAutoPtr.h"

class IFBFunctionBasis;

class IFBFunctionBasisSource
{
public:
  virtual std::vector<FBAutoPtr<IFBFunctionBasis>> CreateBasis() const = 0;
  virtual bool IsFunctionHasConstant() const = 0;

  virtual void IncrementDeg() = 0;
  virtual bool IsDegIncrementRequired(unsigned int iCurrPointCount, unsigned int iRequirePointCount, unsigned int iMaxPointCount) const = 0;

  virtual IFBFunctionBasisSource* CreateCopy() const = 0;
};

#endif // IFBFunctionBasisSource_h
