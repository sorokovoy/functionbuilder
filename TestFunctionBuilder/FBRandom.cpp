// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Define different random generators
//
//===================================================================
//  DEC 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBRandom.h"
#include "FBMathUtils.h"

int r_seed = 123456789;
int r_a = 1103515245;
int r_c = 12345;

int FBRand::BaseRand(void)
{
  r_seed = r_a * r_seed + r_c;
  return FBAbs(r_seed) % FBBaseRandMax;
}

double FBRand::U(double a, double b)
{
  return U01() * ( b - a ) + a;
}

double FBRand::U01(void)
{
  double randVal = (double)BaseRand()/FBBaseRandMax;
  return randVal;
}

double FBRand::N(double s, double a)
{
  double alpha1, alpha2, alpha3;
  double w1, w2, ksi;
  do {
    alpha1 = U01();
    alpha2 = U01();
    w1=2*alpha1-1;
    w2=2*alpha2-1;
  } while (w1*w1+w2*w2<=1);
  alpha3 = U(0.01, 1);

  w1 = w1/sqrt(w1*w1+w2*w2);
  w2 = w2/sqrt(w1*w1+w2*w2);

  ksi = sqrt(-2*log(alpha3))*w1;
  return ( a * ksi + s);
}

unsigned int FBRand::RandInt(unsigned int max)
{
  return BaseRand() % max;
}

bool FBRand::RandBool(void) 
{
  int v = RandInt(3);
  for(int i =0; i < v; i++) {
    BaseRand();
  }
  return BaseRand() % 2 == 0;
}

std::vector<unsigned int> FBRand::GenerateMixedIndexes(unsigned int iIndexMax)
{
  std::list<unsigned int> indexes;
  for(unsigned int i = 0; i < iIndexMax; i++)
    indexes.push_back(i);

  std::vector<unsigned int> result;

  std::list<unsigned int>::iterator it = indexes.begin();
  while(!indexes.empty()) {
    unsigned int steps = FBRand::RandInt(indexes.size());
    for(unsigned int i = 0; i < steps; i++) {
      it ++;
      if(it == indexes.end()) it = indexes.begin();
    }
    result.push_back(*it);
    std::list<unsigned int>::iterator priv = it;
    it++;
    if(it == indexes.end()) it = indexes.begin();
    indexes.erase(priv);
  }
  return result;
}


