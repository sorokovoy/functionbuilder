// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Define different random generators
//
//===================================================================
//  DEC 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBRandom_H
#define FBRandom_H

#include "FBPrecompiled.h"

#define FBBaseRandMax 2147483647

class FBRand
{
public:
  static int BaseRand(void);

  static double N(double E, double Sigma);

  static double U(double a, double b);

  static double U01(void);

//return [0, max)
  static unsigned int RandInt(unsigned int max);

  static bool RandBool(void);

  static std::vector<unsigned int> GenerateMixedIndexes(unsigned int iIndexMax);
};

#endif // FBRandom_H
