// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Function and enum for checking of result codes
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef FBResult_H
#define FBResult_H

enum FBResult {
  rcOk, // method succeeded
  rcFalse, // method succeeded, but result is negative
  rcWrongInput, // incorrect input of method
  rcFailed, // unexpected failure
  rcNotImplemented, // method is not implemented
  rcMemoryAllocation, // out of memory
  rcNotInited, // object initialization fail, or object not init
  rcInvalidOperation, // object operation invalid
  rcXMLConfigError // incorrect format of specified XML element
};

inline bool ok(const FBResult& iRes) { return (iRes == rcOk); }
#endif // FBResult_H
