// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Base class and utilities for create unit tests
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include <vector>
#include "FBUnitTest.h"

#include "TestLinearAlgebra.h"
#include "TestFunctionBuilder.h"


FBResult RunAllUnitTest(void)
{
  std::vector<FBUnitTest*> tests;

  // Add new tests here
  tests.push_back(new TestLinearAlgebra());
  tests.push_back(new TestFunctionBuilder());

  // Run all tests
  int totalTests = 0;
  int failedTests = 0;
  double totalTime = 0;

  for(unsigned int i = 0; i < tests.size(); i++) {
    FBUnitTest* test = tests[i];
    test->Run();
    totalTests += test->GetAllTestsNb();
    failedTests += test->GetFailedTestsNb();
    totalTime += test->GetTotalTime();
    delete test;
  }

  std::cout << "Total tests:\t" << totalTests << std::endl;
  std::cout << "Failed tests:\t" << failedTests << std::endl;
  std::cout << "Total time:\t" << totalTime << std::endl;

  return rcOk;
}