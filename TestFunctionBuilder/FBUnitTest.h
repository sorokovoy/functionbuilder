// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Base class and utilities for create unit tests
//
//===================================================================
//  OCT 2012  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef AUnitTest_H
#define AUnitTest_H

#include <time.h>
#include <iostream>
#include "FBResult.h"
#include "FBMathUtils.h"

FBResult RunAllUnitTest(void);

// abstract class for creating unit test
class FBUnitTest {
public:
  virtual ~FBUnitTest() {};
  virtual FBResult Run() = 0;
  int GetFailedTestsNb() { return _failedTests; }
  int GetAllTestsNb() { return _allTests; }
  double GetTotalTime() {return _totalTime; }
protected:
  FBUnitTest() : _retCode(rcOk), _failedTests(0), _allTests(0), _totalTime(0) {}
  FBResult _retCode;
  int _failedTests;
  int _allTests;
  double _totalTime, _start, _time;
};

#define GetCurrTime ((double)clock()/CLOCKS_PER_SEC)

#define RUN_TEST(func) \
  _start =  GetCurrTime; \
  std::cout <<  std::setw(45) << std::left << #func; \
  _retCode = func; \
  _time = (GetCurrTime - _start); \
  std::cout <<" Time = "<< std::setprecision (3) << std::fixed << std::setw(6) << _time << " -> "; \
  if (_retCode != rcOk) { \
     std::cout << "  FFBL" << std::endl; \
    _retCode = rcFalse; \
    _failedTests++; \
  } \
  else { \
    std::cout << " OK" << std::endl; \
  } \
  _totalTime += _time; \
  _allTests++;

#define ASSERT_OK(arg) \
  if ((arg) != rcOk) { \
    std::cout << "  Error:" << #arg << " is not OK" << std::endl; \
    return rcFalse; \
  }

#define ASSERT_TRUE(arg) \
  if (!(arg)) { \
    std::cout << "  Error:" << #arg << " is FALSE (must be TRUE)" << std::endl; \
    return rcFalse; \
  }

#define ASSERT_FALSE(arg) \
  if (arg) { \
    std::cout << "  Error:" << #arg << " is TRUE (must be FALSE)" << std::endl; \
    return rcFalse; \
  }

#define ASSERT_EQUALS(arg1, arg2) \
  if ((arg1) != (arg2)) { \
    std::cout << "  Error:" << arg1 << " is not equal " << arg2 << std::endl; \
    return rcFalse; \
  }

#define EQUAL_TOLERANCE 1e-6
#define ASSERT_DOUBLE_EQUALS(arg1, arg2) \
  if (!(fabs((arg1) - (arg2)) < EQUAL_TOLERANCE)) { \
    std::cout << "  Error:" << arg1 << " is not equal " << arg2 << std::endl; \
    return rcFalse; \
  }

#define ASSERT_DOUBLE_EQUALS_T(arg1, arg2, iTolerance) \
  if (!(fabs((arg1) - (arg2)) < iTolerance)) { \
    std::cout << "  Error:" << arg1 << " is not equal " << arg2 << std::endl; \
    return rcFalse; \
  }

#endif /* AUnitTest_H */
