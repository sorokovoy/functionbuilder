// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Tests for common algorithms
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "TestFunctionBuilder.h"

#include "FBPolynomialBasis.h"
#include "FBFourierBasis.h"
#include "FBLegendreBasis.h"
#include "FBConstantBasis.h"

#include "FBFunctionBuilder.h"
#include "FBFunction.h"
#include "FBRandom.h"
#include "FBRangeRect.h"

FBResult TestFunctionBuilder::Run()
{
  RUN_TEST(TestFunctionBuilderUnit());
  RUN_TEST(TestFunctionBuilderFuncDerivaty());
  RUN_TEST(TestFunctionSolver());
  RUN_TEST(TestFunctionExtremum());
  RUN_TEST(TestFunctionEvaluatePolinomialExpansion());
  RUN_TEST(TestFunctionMakeVariableReplacement());
  RUN_TEST(TestFunctionSuperposition());
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionBuilderUnit()
{
  ASSERT_OK(TestFunctionBuilderBuildFunction1());
  ASSERT_OK(TestFunctionBuilderBuildFunction2());
  ASSERT_OK(TestFunctionBuilderBuildFunctionLegendre());
  ASSERT_OK(TestFunctionBuilderBuildConstantFunction());
  //ASSERT_OK(TestFunctionBuilderBuildCovariationFunction());

  ASSERT_OK(TestFunctionBuilderPointRelation1());
  ASSERT_OK(TestFunctionBuilderPointRelation2());
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionBuilderFuncDerivaty()
{
  ASSERT_OK(TestPolinomialFuncDerivaty());
  ASSERT_OK(TestForuerFuncDerivaty(eFBFourierBasisTypeOnlyCos));
  ASSERT_OK(TestForuerFuncDerivaty(eFBFourierBasisTypeOnlySin));
  ASSERT_OK(TestForuerFuncDerivaty(eFBFourierBasisTypeSinAndCos));
  ASSERT_OK(TestLegendreFuncDerivaty());
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionSolver()
{
  ASSERT_OK(TestFunctionSolverPolinom());
  ASSERT_OK(TestFunctionSolverTrigonometrical());
  ASSERT_OK(TestFunctionSolverNumerical());
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionEvaluatePolinomialExpansion()
{
  ASSERT_OK(TestFunctionEvaluatePolinomialExpansionUseLegendreMethod());
  ASSERT_OK(TestFunctionEvaluatePolinomialExpansionConvertFromLegendreForm());
  ASSERT_OK(TestFunctionEvaluatePolinomialMultiplication());
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionExtremum()
{
  std::vector<double> polinomDegs;
  polinomDegs.push_back(1);

  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
 // basis.push_back(new FBPolynomialBasis(polinomDegs));
  basis.push_back(new FBFourierBasis(polinomDegs, eFBFourierBasisTypeOnlySin));

  double* coefs = new double[4];
  for(unsigned int i = 0; i < 4; i++) coefs[i] = 1;
  FBFunction f(basis, 2, coefs, false, 0.0001);

  double* mines = new double[2];
  mines[0] = 0;
  mines[1] = 0;

  double* maxes = new double[2];
  maxes[0] = 10;
  maxes[1] = 10;

  FBRangeRect r(mines, maxes, 2, true, TOLERANCE);
  std::string func = f.ToString();
  std::vector<FBFunctionExtremumInfo*> res = f.EvaluateExtremums(&r, eAny, NULL);

  ASSERT_TRUE(res.size() == 5);
  unsigned int maxCount = 0;
  unsigned int minCount = 0;
  for(unsigned int i = 0; i < res.size(); i++) {
    double val = 0;
    ASSERT_TRUE(f.EvaluateFunctionValue(*res[i]->pPoint, val));
    if(res[i]->type == eMax) {
      ASSERT_TRUE(FBEqual(val,  2));
      maxCount ++;
    } else {
      ASSERT_TRUE(FBEqual(val, -2));
      minCount ++;
    }
  }
  ASSERT_TRUE(maxCount == 4);
  ASSERT_TRUE(minCount == 1);
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionSolverPolinom()
{
  std::vector<double> polinomDegs;
  polinomDegs.push_back(1);
  polinomDegs.push_back(2);
  polinomDegs.push_back(3);
  polinomDegs.push_back(0);
  polinomDegs.push_back(4);

  std::vector<FBAutoPtr<IFBFunctionBasis>> polunonialBasis;
  polunonialBasis.push_back(new FBPolynomialBasis(polinomDegs));
  double* coefs = new double[5];
  coefs[0] = 1;
  coefs[1] = -2;
  coefs[2] = -3;
  coefs[3] = 1;
  coefs[4] = 1;

  FBFunction f(polunonialBasis, 1, coefs);
  std::string func = f.ToString();

  std::vector<double> roots = f.EvaluateRoots(-1000, 1000);
  std::sort(roots.begin(), roots.end());

  ASSERT_TRUE(FBEqual(roots[0], 0.685527, 0.00001));
  ASSERT_TRUE(FBEqual(roots[1], 3.46944, 0.00001));
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionSolverTrigonometrical()
{
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionSolverNumerical()
{
  std::vector<double> polinomDegs;
  polinomDegs.push_back(1);
  polinomDegs.push_back(2);
  polinomDegs.push_back(0);

  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  basis.push_back(new FBPolynomialBasis(polinomDegs));
  basis.push_back(new FBFourierBasis(polinomDegs, eFBFourierBasisTypeOnlySin));

  double* coefs = new double[10];
  coefs[0] = 1;
  coefs[1] = -2;
  coefs[2] = -3;
  coefs[3] = 15;
  coefs[4] = 15;
  coefs[5] = -55;
  coefs[6] = -4;
  coefs[7] = -3;
  coefs[8] = 11;
  coefs[9] = 10;

  FBFunction f(basis, 1, coefs);
  std::string func = f.ToString();

  std::vector<double> roots = f.EvaluateRoots(-10, 10);
  std::sort(roots.begin(), roots.end());

  ASSERT_TRUE(FBEqual(roots[0], 0.0655418, 0.00001));
  ASSERT_TRUE(FBEqual(roots[1], 1.80795, 0.00001));
  return rcOk;
}

double polynomFuncDerX1(double x, double y)
{
  return 1 + 4*x + 9*x*x;
}

double polynomFuncDerX2(double x, double y)
{
  return 4 + 10*x + 18*x*x;
}

FBResult TestFunctionBuilder::TestPolinomialFuncDerivaty()
{
  std::vector<double> polinomDegs;
  polinomDegs.push_back(1);
  polinomDegs.push_back(2);
  polinomDegs.push_back(3);

  std::vector<FBAutoPtr<IFBFunctionBasis>> polunonialBasis;
  polunonialBasis.push_back(new FBPolynomialBasis(polinomDegs));
  double* coefs = new double[6];
  for(unsigned int i = 0; i < 6; i++) coefs[i] = i + 1;
  FBFunction f(polunonialBasis, 2, coefs);

  FBAutoPtr<double> data(new double[2]);
  for(unsigned int i = 0; i < 30; i++) {
    data[0] = FBRand::U(0, 100);
    data[1] = data[0];

    double res;
    ASSERT_TRUE(f.EvaluateDirivaryFunction(0)->EvaluateFunctionValue(data.GetConstInstance(), res));
    ASSERT_DOUBLE_EQUALS(res, polynomFuncDerX1(data[0],data[1]));

    ASSERT_TRUE(f.EvaluateDirivaryFunction(1)->EvaluateFunctionValue(data.GetConstInstance(), res));
    ASSERT_DOUBLE_EQUALS(res, polynomFuncDerX2(data[0],data[1]));
  }
  //std::string der0 = f.EvaluateDirivaryFunction(0)->ToString();
  //std::string der1 = f.EvaluateDirivaryFunction(1)->ToString();
  return rcOk;
}

double trigFuncDerX1(double x, double y, FBFourierBasisType type)
{
  switch (type)
  {
  case eFBFourierBasisTypeSinAndCos: return cos(x) - 2*sin(x) + 6*cos(2*x) - 8*sin(2*x) + 15*cos(3*x) - 18*sin(3*x);
  case eFBFourierBasisTypeOnlySin: return cos(x) + 4*cos(2*x) + 9*cos(3*x);
  case eFBFourierBasisTypeOnlyCos: return -sin(x) - 4*sin(2*x) - 9*sin(3*x);
  }
  return 0;
}

double trigFuncDerX2(double y, double x, FBFourierBasisType type)
{
  switch (type)
  {
  case eFBFourierBasisTypeSinAndCos: return 7*cos(x) - 8*sin(x) + 9 * 2 * cos(2 * x) - 10 * 2*sin(2 * x) + 11 * 3 *cos(3*x) - 12 * 3 * sin(3*x);
  case eFBFourierBasisTypeOnlySin: return 4 * cos(x) + 5 * 2 * cos(2*x) +  6 * 3 * cos(3*x);
  case eFBFourierBasisTypeOnlyCos: return - 4 * sin(x) - 5 * 2 * sin(2*x) - 6 * 3 * sin(3*x);
  }
  return 0;
}

FBResult TestFunctionBuilder::TestForuerFuncDerivaty(int type)
{
  std::vector<double> mults;
  mults.push_back(1);
  mults.push_back(2);
  mults.push_back(3);


  double* coefs = new double[12];
  for(unsigned int i = 0; i < 12; i++) coefs[i] = i + 1;

  std::vector<FBAutoPtr<IFBFunctionBasis>> trigBasis;
  trigBasis.push_back(new FBFourierBasis(mults, (FBFourierBasisType) type));

  FBFunction f(trigBasis, 2, coefs);

  FBAutoPtr<double> data(new double[2]);
  for(unsigned int i = 0; i < 30; i++) {

    data[0] = FBRand::U(0, 100);
    data[1] = data[0];

    double res;
    ASSERT_TRUE(f.EvaluateDirivaryFunction(0)->EvaluateFunctionValue(data.GetConstInstance(), res));
   // printf("%s\n", f.EvaluateDirivaryFunction(0)->ToString().c_str());
    ASSERT_DOUBLE_EQUALS(res, trigFuncDerX1(data[0],data[1],(FBFourierBasisType) type));

    ASSERT_TRUE(f.EvaluateDirivaryFunction(1)->EvaluateFunctionValue(data.GetConstInstance(), res));
    ASSERT_DOUBLE_EQUALS(res, trigFuncDerX2(data[0],data[1],(FBFourierBasisType) type));
    data[0] = FBRand::U(0, 100);
    data[1] = FBRand::U(0, 100);
  }
  return rcOk;
}

double LegendreDerivatyFunction(double x)
{
  return 12.7109375 + (10749*x)/64 - (21435*FBPow(x,2))/32 - (53795*FBPow(x,3))/16 + 
   (320985*FBPow(x,4))/64 + (548163*FBPow(x,5))/32 - (363363*FBPow(x,6))/32 - 
   (495495*FBPow(x,7))/16 + (984555*FBPow(x,8))/128 + (1154725*FBPow(x,9))/64;
}

FBResult TestFunctionBuilder::TestLegendreFuncDerivaty()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> legendreBasis;
  legendreBasis.push_back(new FBLegendreBasis(10));
  double* coefs = new double[10];
  for(unsigned int i = 0; i < 10; i++) coefs[i] = i + 1;

  FBFunction f(legendreBasis, 1, coefs);

  FBFunction* derivaty = f.EvaluateDirivaryFunction(0);

  FBAutoPtr<double> data(new double[1]);
  for(unsigned int i = 0; i < 30; i++) {
    data[0] = FBRand::U(0, 10);

    double res;
    ASSERT_TRUE(derivaty->EvaluateFunctionValue(data.GetConstInstance(), res));
    //printf("%s\n", f.EvaluateDirivaryFunction(0)->ToString().c_str());
    ASSERT_DOUBLE_EQUALS_T(res, LegendreDerivatyFunction(data[0]), FBMax(res / 1.00E010, TOLERANCE));
  }
  return rcOk;
}

double f1 (double x, double y)
{
  return -30*x + 2*y + 7 * pow(y, 1.66);
}

FBResult TestFunctionBuilder::TestFunctionBuilderBuildFunction1()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  std::vector<double> arg;
  arg.push_back(1);
  arg.push_back(1.66);
  basis.push_back(new FBPolynomialBasis(arg));

  FBFunctionBuilder b(basis, 2, TOLERANCE, false, 10);

  for(unsigned int i = 0; i < b.GetRequirePointCount() + 1; i++)
  {
    double x, y , fxy;
    x = sin((double)(i * i + 300)) * 0.1 + 1;
    y = cos((double)(i * 100)) * 0.02 + 1;
    fxy = f1(x, y);

    double* item = new double[3];
    item[0] = x;
    item[1] = y;
    item[2] = fxy;
    b.AddApproximationPoint(item);
    //b.EvaluateFunctionValue(item, fxy);
    //printf("%s\n", b.ToString().c_str());
  }

  //printf("%s", b.EvaluateFunction(0.1)->ToString().c_str());

  for(unsigned int i = 10; i < 40; i++)
  {
    double x, y , fxy;
    x = sin((double)(i * i + 300)) * 0.1 + 1;
    y = cos((double)(i * 100)) * 0.02 + 1;

    double* item = new double[3];
    item[0] = x;
    item[1] = y;

    fxy = 0;
    FBFunction* f = b.EvaluateFunction();
    ASSERT_TRUE(f != NULL);
    ASSERT_TRUE(f->EvaluateFunctionValue(item, fxy));
    double realVal = f1(x, y);
    ASSERT_DOUBLE_EQUALS( realVal, fxy);
  }
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionBuilderBuildFunctionLegendre()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  basis.push_back(new FBLegendreBasis(4));

  FBFunctionBuilder b(basis, 2, TOLERANCE, false, 10);

  for(unsigned int i = 0; i < b.GetRequirePointCount(); i++)
  {
    double x, y , fxy;
    x = sin((double)(i * i + 300)) * 0.1 + 1;
    y = cos((double)(i * 100)) * 0.02 + 1;
    fxy = f1(x, y);

    double* item = new double[3];
    item[0] = x;
    item[1] = y;
    item[2] = fxy;
    b.AddApproximationPoint(item);
    //b.EvaluateFunctionValue(item, fxy);
    //printf("%s\n", b.ToString().c_str());
  }

  //printf("%s", b.EvaluateFunction(0.1)->ToString().c_str());

  for(unsigned int i = 10; i < 40; i++)
  {
    double x, y , fxy;
    x = sin((double)(i * i + 300)) * 0.1 + 1;
    y = cos((double)(i * 100)) * 0.02 + 1;

    double* item = new double[3];
    item[0] = x;
    item[1] = y;

    fxy = 0;
    FBFunction* f = b.EvaluateFunction();
    ASSERT_TRUE(f != NULL);
    ASSERT_TRUE(f->EvaluateFunctionValue(item, fxy));
    double realVal = f1(x, y);
    ASSERT_TRUE(FBEqual(realVal, fxy, 0.1));
  }
  return rcOk;
}

double fConst (double x, double y)
{
  return 5;
}

FBResult TestFunctionBuilder::TestFunctionBuilderBuildConstantFunction()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  basis.push_back(new FBConstantBasis());

  FBFunctionBuilder b(basis, 2, TOLERANCE, false, 10);

  for(unsigned int i = 0; i < b.GetRequirePointCount(); i++)
  {
    double x, y , fxy;
    x = sin((double)(i * i + 300)) * 0.1 + 1;
    y = cos((double)(i * 100)) * 0.02 + 1;
    fxy = fConst(x, y);

    double* item = new double[3];
    item[0] = x;
    item[1] = y;
    item[2] = fxy;
    b.AddApproximationPoint(item);
    //b.EvaluateFunctionValue(item, fxy);
    //printf("%s\n", b.ToString().c_str());
  }

  //printf("%s", b.EvaluateFunction(0.1)->ToString().c_str());

  for(unsigned int i = 10; i < 40; i++)
  {
    double x, y , fxy;
    x = sin((double)(i * i + 300)) * 0.1 + 1;
    y = cos((double)(i * 100)) * 0.02 + 1;

    double* item = new double[3];
    item[0] = x;
    item[1] = y;

    FBAutoPtr<const double> value(item);


    fxy = 0;
    FBFunction* f = b.EvaluateFunction();
    ASSERT_TRUE(f != NULL);
    ASSERT_TRUE(f->EvaluateFunctionValue(value, fxy));
    double realVal = fConst(x, y);
    ASSERT_TRUE(FBEqual(realVal, fxy, 0.1));

    FBFunction* derivaty = f->EvaluateDirivaryFunction(0);
    derivaty->EvaluateFunctionValue(value, fxy);
    ASSERT_TRUE(FBEqual(0, fxy, TOLERANCE));
  }
  return rcOk;
}

double f2 (double x, double y)
{
  return  -3*x + 2*y + 3*y*y + 0*x*x + 0*x*x*x + 1;
  return FBRand::U01();
}

FBResult TestFunctionBuilder::TestFunctionBuilderBuildFunction2()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  std::vector<double> arg;
  arg.push_back(1);
  arg.push_back(2);
  //arg.push_back(3);
  basis.push_back(new FBPolynomialBasis(arg));
  //basis.push_back(new FBLegendreBasis(2));
  FBFunctionBuilder b(basis, 2, TOLERANCE, true, 6);

  for(unsigned int i = 0; i < b.GetRequirePointCount() * 300; i++)
  {
    double x, y , fxy;
    x = sin((double)(i * i + 300)) + 1.1;
    y = cos((double)(i * 100))  + 1.1 ;

    fxy = f2(x, y);

    double* item = new double[3];
    item[0] = x;
    item[1] = y;
    item[2] = fxy;
    b.AddApproximationPoint(item);
    b.EvaluateFunction(0.1);
    //b.EvaluateFunctionValue(item, fxy);
    //printf("%s\n", b.ToString().c_str());
  }

 // double err = b.CalculateAverageError();
 // printf("%s", b.EvaluateFunction(0.1)->ToString().c_str());

  for(unsigned int i = 10; i < 40; i++)
  {
    double x, y , fxy;
    x = sin((double)(i * i + 300)) * 0.1 + 1;
    y = cos((double)(i * 100)) * 0.02 + 1;

    double* item = new double[3];
    item[0] = x;
    item[1] = y;

    fxy = 0;
    FBFunction* f = b.EvaluateFunction(1);
    ASSERT_TRUE(f != NULL);
    ASSERT_TRUE(f->EvaluateFunctionValue(item, fxy));
    double realVal = f2(x, y);
    ASSERT_DOUBLE_EQUALS(realVal, fxy);
  }
  //printf("%s", b.EvaluateFunction()->ToString().c_str());
  return rcOk;
}


FBResult TestFunctionBuilder::TestFunctionBuilderPointRelation1()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  std::vector<double> arg;
  arg.push_back(1);
  arg.push_back(2);
  basis.push_back(new FBPolynomialBasis(arg));

  FBFunctionBuilder b(basis, 2, 0.2, false, 1000);

  double* item;
  for(unsigned int i = 0; i < b.GetRequirePointCount() * 1000; i++)
  {
    item = new double[3];
    item[0] = (sin((double)(i * i + 300)) + 3);
    item[1] = (FBRand::RandBool() ? 1 : -1)*sqrt(1 - (item[0]-3)*(item[0]-3) ) + 3;
    item[2] = 1;

    item[0] += FBRand::N(0, 1.5);
    item[1] += FBRand::N(0, 1.5);

    b.AddApproximationPoint(item);
    //b.EvaluateFunctionValue(item, fxy);
    //printf("%s\n", b.ToString().c_str());
  }

  FBFunction* f = b.EvaluateFunction();
  ASSERT_TRUE(f != NULL);
  //printf("%s\n", f->ToString().c_str());
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionBuilderPointRelation2()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  std::vector<double> arg;
  arg.push_back(1);
  arg.push_back(2);
  arg.push_back(3);
  basis.push_back(new FBPolynomialBasis(arg));

  FBFunctionBuilder b(basis, 2, 0.002, false, 10);

  double* item;
  for(unsigned int i = 0; i < 6; i++)
  {
    item = new double[3];
    item[0] = 1 + i/2;
    item[1] = 1 + i/3;
    item[2] = 1;

    b.AddApproximationPoint(item);
    //b.EvaluateFunctionValue(item, fxy);
    //printf("%s\n", b.ToString().c_str());
  }

  FBFunction* f = b.EvaluateFunction();
  ASSERT_TRUE(f != NULL);
  //printf("%s\n", b->ToString().c_str());
  return rcOk;
}

double superPositionFunc(double x, double y)
{
  double fVal = x + FBSqr(x) + y + FBSqr(y);
  return 2 * (fVal + FBSqr(fVal));
}

FBResult TestFunctionBuilder::TestFunctionSuperposition()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis1;
  basis1.push_back(new FBPolynomialBasis(2));

  double* coefs = new double[4];
  for(unsigned int i = 0; i < 4; i++) coefs[i] = 1 + i;

  FBFunction fBase(basis1, 2, coefs, false, 0.0001);

  double* pMaxes = new double[2];
  double* pMines = new double[2];

  for(unsigned int i = 0; i < 2; i++) {
    pMaxes[i] = 1;
    pMines[i] = 0;
  }

  std::vector<FBFunction*> argFuncs;
  for(int i = 0; i < 2; i++) {
    double* coefs1 = new double[4];
    for(unsigned int i = 0; i < 4; i++) coefs1[i] = 1 + i;
    argFuncs.push_back(new FBFunction(basis1, 2, coefs1, false, 0.0001));
  }

  FBFunction* superposition = fBase.CreateSuperpositionFunc(argFuncs, &FBRangeRect(pMines, pMaxes, 2, false, TOLERANCE));
  //printf("interp1 = %s\n", superposition->ToString().c_str());


  for(unsigned int i = 0; i < 10; i++) {
    double arg[2];
    arg[0] = FBRand::U(0, 1);
    arg[1] = FBRand::U(0, 1);

    double innerArg[100];
    for(unsigned int i = 0; i < argFuncs.size(); i++)
      innerArg[i] = argFuncs[i]->EvaluateFunctionValue(arg);

    double fSuperposisiton = fBase.EvaluateFunctionValue(innerArg);
    double fres;
    ASSERT_TRUE(superposition->EvaluateFunctionValue(arg, fres));
    ASSERT_DOUBLE_EQUALS_T(fres, fSuperposisiton, 140);
  }

  delete pMaxes;
  delete pMines;
  delete superposition;
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionEvaluatePolinomialExpansionUseLegendreMethod()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis1;

  unsigned int varCount = 2;
  unsigned int basisSize = 2;
  double rangeMax = 10;

  basis1.push_back(new FBPolynomialBasis(basisSize));

  double* coefs = new double[varCount * basisSize];
  for(unsigned int i = 0; i < varCount * basisSize; i++) coefs[i] = i + 1;

  FBFunction fBase(basis1, varCount, coefs, false, 0.0001);

  double* mines = new double[varCount];
  double* maxes = new double[varCount];

  for(unsigned int i = 0; i < varCount; i++) {
    mines[i] = -1;
    maxes[i] = rangeMax;
  }

  FBRangeRect range(mines, maxes, varCount, true, 0.0001);

  FBFunction* decomposition = fBase.CreatePolynomialExpansionUsingLegendrePolinoms(&range);
  //printf("D=%s\n", decomposition->ToString().c_str());

  FBAutoPtr<double> ipArg = FBAutoPtr<double>(new double[varCount]);
  for(unsigned int i = 0; i < 10; i++) {
    for(unsigned int i = 0; i < varCount; i++) {
      ipArg[i] = FBRand::U(0, rangeMax);
    }
    double decompositionValue = decomposition->EvaluateFunctionValue(ipArg);
    ASSERT_DOUBLE_EQUALS_T(decompositionValue, fBase.EvaluateFunctionValue(ipArg), decompositionValue/100);
  }

  delete decomposition;
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionEvaluatePolinomialExpansionConvertFromLegendreForm()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis1;

  unsigned int varCount = 2;
  unsigned int basisSize = 2;
  double rangeMax = 10;

  basis1.push_back(new FBLegendreBasis(basisSize));

  double* coefs = new double[varCount * basisSize];
  for(unsigned int i = 0; i < varCount * basisSize; i++) coefs[i] = i + 1;

  FBFunction fBase(basis1, varCount, coefs, false, 0.0001);

  FBFunction* decomposition = fBase.CreatePolynomialExpansion(NULL);
  //printf("D=%s\n", decomposition->ToString().c_str());

  FBAutoPtr<double> ipArg = FBAutoPtr<double>(new double[varCount]);
  for(unsigned int i = 0; i < 10; i++) {
    for(unsigned int i = 0; i < varCount; i++) {
      ipArg[i] = FBRand::U(-100, 100);
    }
    double decompositionValue = decomposition->EvaluateFunctionValue(ipArg);
    ASSERT_DOUBLE_EQUALS_T(decompositionValue, fBase.EvaluateFunctionValue(ipArg), decompositionValue/100);
  }

  delete decomposition;
  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionEvaluatePolinomialMultiplication()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis1;
  unsigned int basisSize = 10;

  basis1.push_back(new FBPolynomialBasis(basisSize));

  double* coefs1 = new double[basisSize + 1];
  double* coefs2 = new double[basisSize + 1];

  double* fcoefs1 = new double[basisSize + 1];
  double* fcoefs2 = new double[basisSize + 1];


  for(unsigned int i = 0; i < basisSize + 1; i++) {
    coefs1[i] = i;
    coefs2[i] = i;

    fcoefs1[i] = i + 1;
    fcoefs2[i] = i + 1;
  }

  FBFunction f1(basis1, 1, fcoefs1, false, 0.0001);
  FBFunction f2(basis1, 1, fcoefs2, false, 0.0001);

  double* multCoef = new double[basisSize * 2 + 2];
  unsigned int newDeg = FBEvaluatePolynomialMult(coefs1, basisSize, coefs2, basisSize, multCoef);

  std::vector<FBAutoPtr<IFBFunctionBasis>> basis2;
  basis2.push_back(new FBPolynomialBasis(newDeg));

  for(unsigned int i = 0; i < newDeg; i ++) {
    multCoef[i] = multCoef[i + 1];
  }

  FBFunction fRes(basis2, 1, multCoef, false, 0.0001);

  double x;
  for(unsigned int i = 0; i < 10; i++) {
    x = FBRand::U(0, 1);

    double f1Value = f1.EvaluateFunctionValue(&x);
    double f2Value = f2.EvaluateFunctionValue(&x);
    double f1f2Value = fRes.EvaluateFunctionValue(&x);

    ASSERT_DOUBLE_EQUALS_T(f1Value * f2Value, f1f2Value, f1f2Value / 10000);
  }

  delete coefs1;
  delete coefs2;

  return rcOk;
}

FBResult TestFunctionBuilder::TestFunctionMakeVariableReplacement()
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis1;

  unsigned int varCount = 2;
  unsigned int basisSize = 2;
  double rangeMax = 10;

  basis1.push_back(new FBPolynomialBasis(basisSize));

  double* coefs = new double[varCount * basisSize];
  for(unsigned int i = 0; i < varCount * basisSize; i++) coefs[i] = i + 1;

  FBFunction fBase(basis1, varCount, coefs, false, 0.0001);

  double* ra = new double[varCount];
  double* rb = new double[varCount];

  for(unsigned int i = 0; i < varCount; i++) {
    ra[i] = 2;
    rb[i] = -2;
  }

  FBFunction* pReplace = fBase.EvaluateVariableReplace(ra, rb);

  double* fArg = new double[varCount];
  for(unsigned int i = 0; i < 10; i++) {
    for(unsigned int j = 0; j < varCount; j++) {
      fArg[j] = FBRand::U(0, 1);
    }

    double fValue = pReplace->EvaluateFunctionValue(fArg);
    for(unsigned int j = 0; j < varCount; j++) {
      fArg[j] = ra[j] * fArg[j] + rb[j];
    }
    double targetVal = fBase.EvaluateFunctionValue(fArg);

    ASSERT_DOUBLE_EQUALS_T(fValue, targetVal, TOLERANCE);
  }

  delete ra;
  delete rb;
  delete fArg;
  delete pReplace;

  return rcOk;
}


