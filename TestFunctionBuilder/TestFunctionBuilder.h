// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Tests for common algorithms
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef TestFunctionBuilder_H
#define TestFunctionBuilder_H

#include "FBUnitTest.h"

class TestFunctionBuilder : public FBUnitTest
{
public:
  virtual FBResult Run();

private:
  FBResult TestFunctionBuilderUnit();
  FBResult TestFunctionBuilderFuncDerivaty();
  FBResult TestFunctionSolver();
  FBResult TestFunctionExtremum();

  FBResult TestFunctionSolverPolinom();
  FBResult TestFunctionSolverTrigonometrical();
  FBResult TestFunctionSolverNumerical();

  FBResult TestFunctionSuperposition();
  FBResult TestFunctionPolynomialExpansion();

  FBResult TestPolinomialFuncDerivaty();
  FBResult TestForuerFuncDerivaty(int type);
  FBResult TestLegendreFuncDerivaty();

  FBResult TestFunctionBuilderBuildFunction1();
  FBResult TestFunctionBuilderBuildFunction2();
  FBResult TestFunctionBuilderBuildFunctionLegendre();
  FBResult TestFunctionBuilderBuildConstantFunction();

  //FBResult TestFunctionBuilderBuildCovariationFunction();

  FBResult TestFunctionBuilderPointRelation1();
  FBResult TestFunctionBuilderPointRelation2();

  FBResult TestFunctionEvaluatePolinomialExpansion();

  FBResult TestFunctionEvaluatePolinomialExpansionUseLegendreMethod();
  FBResult TestFunctionEvaluatePolinomialExpansionConvertFromLegendreForm();

  FBResult TestFunctionEvaluatePolinomialMultiplication();
  FBResult TestFunctionMakeVariableReplacement();
};

#endif // TestFunctionBuilder_H
