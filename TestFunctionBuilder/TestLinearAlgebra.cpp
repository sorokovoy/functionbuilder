// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Tests and example of using linear algebra lib.
// Now using Eigen: http://eigen.tuxfamily.org/index.php?title=Main_Page
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#include "FBPrecompiled.h"

#include "TestLinearAlgebra.h"

#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/SVD>

using namespace Eigen;
using namespace std;

FBResult TestLinearAlgebra::Run()
{
  RUN_TEST(TestLinearSolverLU());
  RUN_TEST(TestLinearSolverSVD());
  RUN_TEST(TestMatrixValueCalculation());
  return rcOk;
}

FBResult TestLinearAlgebra::TestLinearSolverSVD()
{
  MatrixXf A = MatrixXf::Random(20, 20);
  A(0,0) = 0;
  VectorXf b = VectorXf::Random(20);
  Eigen::JacobiSVD<Eigen::MatrixXf> svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
  VectorXf x = svd.solve(b);
  double err = (A * x - b).norm();
  ASSERT_TRUE(err < 0.1);
  return rcOk;
}

FBResult TestLinearAlgebra::TestLinearSolverLU()
{
  MatrixXf A = MatrixXf::Random(20, 20);
  A(0,0) = 0;
  VectorXf b = VectorXf::Random(20);
  VectorXf x = A.lu().solve(b);
  double err = (A * x - b).norm();
  ASSERT_TRUE(err < 0.1);
  return rcOk;
}

FBResult TestLinearAlgebra::TestMatrixValueCalculation()
{
  MatrixXf A = MatrixXf::Random(20, 20);
  double det = A.determinant();
 // cout << "det is " << det << endl;

  JacobiSVD<MatrixXf> svd(A);
  double cond = svd.singularValues()(0) / svd.singularValues()(svd.singularValues().size()-1);
//  cout << "cond is " << cond << endl;

  return rcOk;
}
