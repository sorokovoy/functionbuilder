// COPYRIGHT: Open source by Academic Free License ("AFL") v. 3.0
//===================================================================
//
// Tests and example of using linear algebra lib.
// Now using Eigen: http://eigen.tuxfamily.org/index.php?title=Main_Page
//
//===================================================================
//  AUG 2013  Creation:                       Sorokovoy Alexey, NSU
//===================================================================

#ifndef TestLinearAlgebra_H
#define TestLinearAlgebra_H

#include "FBUnitTest.h"

class TestLinearAlgebra : public FBUnitTest
{
public:
  virtual FBResult Run();

private:
  FBResult TestLinearSolverLU();
  FBResult TestLinearSolverSVD();
  FBResult TestMatrixValueCalculation();
};

#endif // TestLinearAlgebra_H
