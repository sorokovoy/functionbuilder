
#include "FBUnitTest.h"

#include "FBPolynomialBasis.h"
#include "FBFunction.h"
#include "FBFunctionBuilder.h"
#include "FBRandom.h"

#include "FBArrayAgregator.h"

void RunRuncTest(double* coefs, bool iIsContainsConstant, unsigned int iVarCount, std::vector<double> iDegs, double iErrorDispersion, unsigned int iPoints);

void ProcessFunction(std::vector<double> iDegs, unsigned int varCount, bool isContaisnsConstant);

void main(void)
{
  //RunAllUnitTest();

  std::vector<double> degs;
  degs.push_back(1);
  degs.push_back(2);
  ProcessFunction(degs, 1, false);

  degs.clear();
  degs.push_back(1);
  degs.push_back(2);
  ProcessFunction(degs, 2, false);

  degs.clear();
  degs.push_back(1);
  degs.push_back(2);
  degs.push_back(3);
  ProcessFunction(degs, 2, true);


  degs.clear();
  degs.push_back(0.5);
  degs.push_back(1.5);
  degs.push_back(2.5);
  ProcessFunction(degs, 2, true);

  
  degs.clear();
  degs.push_back(1);
  degs.push_back(2);
  degs.push_back(3);
  ProcessFunction(degs, 3, false);

}

int call = 0;

void ProcessFunction(std::vector<double> degs, unsigned int varCount, bool isConstainsConstant)
{
  unsigned int coefCount = degs.size() * varCount + (isConstainsConstant ? 1 : 0);

  call++;
  //printf("%d &", call);


  printf("\item  $", call);

  double* coefs = new double[coefCount];
  for(unsigned int i = 0; i < coefCount; i++) {
    int v = FBRand::RandInt(5);
    coefs[i] = v  - 2;
    if(isConstainsConstant && i == coefCount - 1) {
      continue;
    }

    if(FBAbs(coefs[i]) < TOLERANCE) {
      continue;
    }
    if(i != 0 && coefs[i] > 0) {
      printf(" + ");
    }

    if(fmod(degs[i/varCount], 1.0) > TOLERANCE) {
      printf("%.0fx_%d^{%.1f}",  coefs[i], i / degs.size(), degs[i/varCount]);
    } else {
      printf("%.0fx_%d^{%.0f}",  coefs[i], i / degs.size(), degs[i/varCount]);
    }
  }
  
  if(isConstainsConstant) {
    if(coefs[coefCount - 1] > 0 ) {
      printf(" + %.0f", coefs[coefCount - 1]);
    } else {
      printf(" %.0f", coefs[coefCount - 1]);
    }
  }
  printf("$ \n ");
  


  /*
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 0.0, 0);
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 0.001, 100);
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 0.001, 1000);
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 0.01, 100);
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 0.01, 1000);

  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 0.1, 1000);
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 0.1, 10000);
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 0.1, 100000);
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 1, 100000);
  RunRuncTest(coefs, isConstainsConstant, varCount, degs, 1, 1000000);
*/
 // printf("\\ \n");
}

void RunRuncTest(double* coefs, bool iIsContainsConstant, unsigned int iVarCount, std::vector<double> iDegs, double iErrorDispersion, unsigned int iPoints)
{
  std::vector<FBAutoPtr<IFBFunctionBasis>> basis;
  basis.push_back(new FBPolynomialBasis(iDegs));

  FBFunctionBuilder b(basis, iVarCount, TOLERANCE, iIsContainsConstant);
  unsigned int iCoefCount = iVarCount * iDegs.size() + (iIsContainsConstant ? 1 : 0);

  double* pCoefClone = new double[iCoefCount];
  memcpy(pCoefClone, coefs, sizeof(double) * iCoefCount);

  FBFunction targetFunc(basis, iVarCount, pCoefClone, iIsContainsConstant, TOLERANCE);

  double* pArg = new double[iVarCount];

  double uMin = 1;
  double uMax = 10;

  for(unsigned int i = 0; i < FBMax(iPoints, b.GetRequirePointCount() + 3); i++)
  {
    for(unsigned int j = 0; j < iVarCount; j++) {
      pArg[j] = FBRand::U(uMin, uMax); // use positive value arg for avoid evaluaton eerors on fructure degs
    }

    double* item = new double[iVarCount + 1];
    memcpy(item, pArg, sizeof(double) * iVarCount);
    double fvale = targetFunc.EvaluateFunctionValue(pArg);
    item[iVarCount] = fvale + FBRand::N(0, FBAbs(fvale) * iErrorDispersion);

    b.AddApproximationPoint(item);
    //b.EvaluateFunctionValue(item, fxy);
    //printf("%s\n", b.ToString().c_str());
  }

  FBFunction* pFunc = b.EvaluateFunction(10000000000);
  const double* evaluatedCoefs = pFunc->GetRawCoefs();


  FBArrayAgregator<double> agr; 

  for(unsigned int i = 0; i < iCoefCount; i++) {
    agr.ProcessValue(FBAbs(evaluatedCoefs[i] - coefs[i]));
  }

  FBArrayAgregator<double> val_agr;
  FBArrayAgregator<double> abs_val_agr;

  unsigned int* indexes = new unsigned int[iVarCount];
  for(unsigned int i = 0; i < iVarCount; i++) {
    indexes[i] = 0;
    pArg[i] = uMin;
  }
  unsigned int maxIndex = 20;

  double step = (uMax - uMin) / maxIndex;

  while(true) {
    val_agr.ProcessValue(FBAbs(pFunc->EvaluateFunctionValue(pArg) - targetFunc.EvaluateFunctionValue(pArg))/ FBMax( targetFunc.EvaluateFunctionValue(pArg), 1));
    abs_val_agr.ProcessValue(FBAbs(targetFunc.EvaluateFunctionValue(pArg)));

    unsigned int incrementedIndex = 0;
    while(indexes[incrementedIndex] + 1 == maxIndex) {
      indexes[incrementedIndex] = 0;
      pArg[incrementedIndex] = uMin;
      incrementedIndex++;

      if(incrementedIndex == iVarCount) {
        break;
      }
    }
    if(incrementedIndex == iVarCount) {
      break;
    }
    indexes[incrementedIndex] ++;
    pArg[incrementedIndex] += step;
  }

  printf(" %.2f/%02.0f/%02.0f &", agr.GetMax(), FBMin(val_agr.GetMax()* 100, 99), FBMin(val_agr.GetAverage() * 100, 99));

}